#ifndef MAZE_H
#define MAZE_H

#include <vector>

class Cell
{
public:

    bool visited;

    bool topWall;
    bool bottomWall;
    bool leftWall;
    bool rightWall;

    Cell();
};

enum class Direction
{
    Right, // positive x
    Up,    // negative y
    Left,
    Down
};

Direction clockwise(Direction direction);
Direction counterClockwise(Direction direction);

class RowCol
{
public:
    int row;
    int col;

    RowCol toLeft() { return { row, col-1}; }
    RowCol toRight() { return { row, col+1}; }
    RowCol above() { return { row-1, col}; }
    RowCol below() { return { row+1, col}; }
};

class Maze
{
    std::vector<Cell> _cells;
    int _width;
    int _height;
public:
    Maze(int width, int height);
   ~Maze();
    void setWall(RowCol position, Direction direction, bool hasWall);
    bool hasWall(RowCol position, Direction direction);
    int  width();
    int  height();

    void buildMaze();
    void buildMazeRecursive(RowCol position);

    bool isValidMove(RowCol position, Direction direction);

    bool solveRecursive(RowCol position, Direction moved, std::vector<RowCol>& path);
    void solveMaze(std::vector<RowCol>& path);

private:
    Cell &cell(RowCol position);
public:
    bool testWalls();
};

#endif // MAZE_H
