#include "catch.hpp"
#include "mystack.h"

TEST_CASE( "some tests of our stack", "[vector]" ) {

    MyStack stack;

    CHECK( stack.isEmpty() == true );

    SECTION( "pushing items" ) {

        stack.push(37);

        CHECK( stack.peek() == 37 );

        REQUIRE( stack.isEmpty() == false );
    }

    SECTION( "pushing and popping item" ) {

        stack.push(37);

        auto value = stack.pop();

        REQUIRE( value == 37 );

        REQUIRE (stack.isEmpty() == true);
    }

    SECTION( "pushing and popping multiple items" ) {

        for (int i=0;i<50;i++)
        {
            stack.push(i);

            REQUIRE( stack.peek() == i );
        }

        for (int i=49;i >= 0;i--)
        {
            auto value = stack.pop();

            REQUIRE(value == i);
        }


        REQUIRE (stack.isEmpty() == true);
    }


}
