

#include <chrono>
#include <string>
#include <thread>
#include <cmath>

#include "graphics.h"
#include "internal.h"
#include "font.h"

using namespace std;

extern Vec2d mousePos;

void drawLine(Vec2d p1, Vec2d p2, unsigned int color)
{
    glBegin(GL_LINES);
    glColor3ub((color >> 16 & 0xFF), (color >> 8 & 0xFF), (color & 0xFF));
    glVertex2d(p1.x, p1.y);
    glVertex2d(p2.x, p2.y);
    glEnd();
}

void drawPoint(Vec2d Vec2d, unsigned int color)
{
    glPointSize(3);
    glBegin(GL_POINTS);
    glColor3ub((color >> 16 & 0xFF), (color >> 8 & 0xFF), (color & 0xFF));
    glVertex2d(Vec2d.x, Vec2d.y);
    glEnd();
    glPointSize(1);
}

void drawPolyline(const std::vector<Vec2d>& pts, unsigned int color)
{
    glBegin(GL_LINE_STRIP);
    glColor3ub((color >> 16 & 0xFF), (color >> 8 & 0xFF), (color & 0xFF));
    for (auto& p : pts)
    {
        glVertex2d(p.x, p.y);
    }
    glEnd();
}

void drawPolyline(std::vector<Vec2d> pts, Vec2d offset, double scale, double angle, unsigned int color)
{
    glBegin(GL_LINE_STRIP);
    glColor3ub((color >> 16 & 0xFF), (color >> 8 & 0xFF), (color & 0xFF));
    for (auto& p : pts)
    {
        if (scale != 1)
        {
            p.scale(scale);
        }
        if (angle != 0)
        {
            p.rotate(angle);
        }
        p = p + offset;
        glVertex2d(p.x, p.y);
    }
    glEnd();
}

void drawPolygon(const std::vector<Vec2d>& pts, unsigned int color)
{
    glBegin(GL_LINE_STRIP);
    glColor3ub((color >> 16 & 0xFF), (color >> 8 & 0xFF), (color & 0xFF));
    for (auto& p : pts)
    {
        glVertex2d(p.x, p.y);
    }
    glVertex2d(pts[0].x, pts[0].y);
    glEnd();
}

void drawPolygon(std::vector<Vec2d> pts, Vec2d offset, double scale, double angle, unsigned int color)
{
    glBegin(GL_LINE_STRIP);
    glColor3ub((color >> 16 & 0xFF), (color >> 8 & 0xFF), (color & 0xFF));
    pts.push_back(pts[0]);
    for (auto& p : pts)
    {
        if (scale != 1)
        {
            p.scale(scale);
        }
        if (angle != 0)
        {
            p.rotate(angle);
        }
        p = p + offset;
        glVertex2d(p.x, p.y);
    }
    glEnd();
}

void fillPolygon(const std::vector<Vec2d>& pts, unsigned int color)
{
    glBegin(GL_POLYGON);
    glColor3ub((color >> 16 & 0xFF), (color >> 8 & 0xFF), (color & 0xFF));
    for (auto& p : pts)
    {
        glVertex2d(p.x, p.y);
    }
    glEnd();
}

void fillPolygon(std::vector<Vec2d> pts, Vec2d offset, double scale, double angle, unsigned int color)
{
    glBegin(GL_POLYGON);
    glColor3ub((color >> 16 & 0xFF), (color >> 8 & 0xFF), (color & 0xFF));
    for (auto& p : pts)
    {
        if (scale != 1)
        {
            p.scale(scale);
        }
        if (angle != 0)
        {
            p.rotate(angle);
        }
        p = p + offset;
        glVertex2d(p.x, p.y);
    }
    glEnd();
}

void drawEllipse(Vec2d center, double width, double height, double angle, unsigned int color)
{
    width  /= 2;
    height /= 2;

    constexpr int    numSteps = 32;
    constexpr double stepSize = 2.0*M_PI/numSteps;

    constexpr double sinang = sin(stepSize);
    constexpr double cosang = cos(stepSize);

    double sinrot = sin(angle);
    double cosrot = cos(angle);

    glBegin(GL_LINE_LOOP);
    glColor3ub((color >> 16 & 0xFF), (color >> 8 & 0xFF), (color & 0xFF));
    Vec2d pt{1.0, 0};

    for (int i=0; i<=numSteps; ++i)
    {
        Vec2d p { pt.x * width, pt.y * height };
        p = { p.x * cosrot - p.y * sinrot + center.x, p.x * sinrot + p.y * cosrot + center.y };
        glVertex2d(p.x, p.y);
        pt = { pt.x * cosang - pt.y * sinang, pt.x * sinang + pt.y * cosang };
    }

    glEnd();
}

void fillEllipse(Vec2d center, double width, double height, double angle, unsigned int color)
{
    width  /= 2;
    height /= 2;

    constexpr int    numSteps = 32;
    constexpr double stepSize = 2.0*M_PI/numSteps;

    constexpr double sinang = sin(stepSize);
    constexpr double cosang = cos(stepSize);

    double sinrot = sin(angle);
    double cosrot = cos(angle);

    glBegin(GL_TRIANGLE_FAN);
    glColor3ub((color >> 16 & 0xFF), (color >> 8 & 0xFF), (color & 0xFF));
    glVertex2d(center.x, center.y);

    Vec2d pt{1.0, 0};

    for (int i=0; i<=numSteps; ++i)
    {
        Vec2d p { pt.x * width, pt.y * height };
        p = { p.x * cosrot - p.y * sinrot + center.x, p.x * sinrot + p.y * cosrot + center.y };
        glVertex2d(p.x, p.y);
        pt = { pt.x * cosang - pt.y * sinang, pt.x * sinang + pt.y * cosang };
    }

    glEnd();
}


void drawString(Vec2d position, string text, double scale, unsigned int color)
{
    glPushMatrix();
    glColor3ub((color >> 16 & 0xFF), (color >> 8 & 0xFF), (color & 0xFF));
    glTranslatef(position.x, position.y, 0);
    glScalef(scale, scale, scale);
    mssmStrokeString(false, text.c_str());
    glPopMatrix();
}

void displayCallback(int width, int height, int framePeriodMs)
{
    int w = width/2;
    int h = height/2;

    static auto lastTime = std::chrono::steady_clock::now();

    auto currentTime = std::chrono::steady_clock::now();

    int elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - lastTime).count();

    lastTime = currentTime;

    // clear the screen
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);

    // set up the projection matrix (projects the 3d Vec2ds onto the 2d screen)
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-w, w, -h, h, -20.0, 500.0);
    //glFrustrum()

    // set up the model-to-view matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // call our function that actually displays stuff
    updateDisplay(elapsedTime);

    // trigger display update at our selected framerate
    if (framePeriodMs != 0)
    {
        std::this_thread::sleep_until(lastTime + std::chrono::milliseconds(framePeriodMs));
    }
}




Window::~Window()
{
}





Window::Window(const std::string &windowName, int width, int height, int frameRate)
{
    glWidget = new GLWidget(width, height, frameRate);
    glFont = new QFont("Times");
    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->addWidget(glWidget);
    mainLayout->setContentsMargins(0,0,0,0);
    setLayout(mainLayout);

    setWindowTitle(windowName.c_str());
}

void Window::renderText(double x, double y, std::string text)
{
    this->glWidget->renderText(x, y, 0, text.c_str(), *glFont);
}

void Window::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape)
        close();
    else
        QWidget::keyPressEvent(e);
}

#ifndef GL_MULTISAMPLE
#define GL_MULTISAMPLE  0x809D
#endif

GLWidget::GLWidget(int width, int height, int frameRate)
    : QGLWidget(QGLFormat(QGL::SampleBuffers), nullptr)
{
    _width = width;
    _height = height;
    _frameRate = frameRate;
    _framePeriodMs = frameRate == 0 ? 0 : 1000/frameRate;

    if (_framePeriodMs)
    {
        startTimer(_framePeriodMs);
    }

    setMouseTracking(true);
    setFocusPolicy(Qt::StrongFocus);
}

GLWidget::~GLWidget()
{
}

QSize GLWidget::minimumSizeHint() const
{
    return QSize(_width, _height);
}
//! [2]

//! [3]
QSize GLWidget::sizeHint() const
//! [3] //! [4]
{
    return QSize(_width, _height);
}
//! [4]




//! [6]
void GLWidget::initializeGL()
{
    //qglClearColor(qtPurple.dark());

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    /*
    glShadeModel(GL_SMOOTH);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_MULTISAMPLE);
    static GLfloat lightAmbient[4] = { 1.0, 1.0, 1.0, 1.0 };
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lightAmbient);
    static GLfloat lightPosition[4] = { 0.5, 5.0, 7.0, 1.0 };
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
    */

}

void GLWidget::timerEvent(QTimerEvent* /*event*/)
{
    updateGL();
}

void GLWidget::paintGL()
{
    displayCallback(_width, _height,_framePeriodMs);
}

void GLWidget::resizeGL(int width, int height)
{
    _actualWidth  = width;
    _actualHeight = height;
    glViewport(1,1,width-1,height-1);
}

void GLWidget::keyPressEvent(QKeyEvent *event)
{
    if (event->text().size() > 0)
    {
        keyboardFunc(event->text().at(0).toLatin1(), _lastMouseX, _lastMouseY);
    }
}

void GLWidget::keyReleaseEvent(QKeyEvent* event)
{
    if (event->text().size() > 0)
    {
        keyboardUpFunc(event->text().at(0).toLatin1(), _lastMouseX, _lastMouseY);
    }
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    _lastMouseX = (double)(event->x()-_actualWidth/2)*_width/_actualWidth;
    _lastMouseY = (double)(_actualHeight/2-event->y())*_height/_actualHeight;

    mousePos.x = _lastMouseX;
    mousePos.y = _lastMouseY;

    switch (event->button())
    {
    default:
    case Qt::NoButton:
        break;
    case Qt::LeftButton:
        mouseFunc(0, 0, _lastMouseX, _lastMouseY);
        break;
    case Qt::RightButton:
        mouseFunc(2, 0, _lastMouseX, _lastMouseY);
        break;
    case Qt::MiddleButton:
        mouseFunc(1, 0, _lastMouseX, _lastMouseY);
        break;
    }
}

void GLWidget::mouseReleaseEvent(QMouseEvent *event)
{
    _lastMouseX = (double)(event->x()-_actualWidth/2)*_width/_actualWidth;
    _lastMouseY = (double)(_actualHeight/2-event->y())*_height/_actualHeight;

    mousePos.x = _lastMouseX;
    mousePos.y = _lastMouseY;

    switch (event->button())
    {
    default:
    case Qt::NoButton:
        break;
    case Qt::LeftButton:
        mouseFunc(0, 1, _lastMouseX, _lastMouseY);
        break;
    case Qt::RightButton:
        mouseFunc(2, 1, _lastMouseX, _lastMouseY);
        break;
    case Qt::MiddleButton:
        mouseFunc(1, 1, _lastMouseX, _lastMouseY);
        break;
    }
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    _lastMouseX = (double)(event->x()-_actualWidth/2)*_width/_actualWidth;
    _lastMouseY = (double)(_actualHeight/2-event->y())*_height/_actualHeight;

    mousePos.x = _lastMouseX;
    mousePos.y = _lastMouseY;

    if (event->buttons() & (Qt::LeftButton | Qt::RightButton))
    {
        motionFunc(_lastMouseX, _lastMouseY);
    }
    else
    {
        passiveMotionFunc(_lastMouseX, _lastMouseY);
    }
}


void beginGraphicsLoop(const std::string &name, int windowWidth, int windowHeight, int frameRate)
{
    int argc = 0;
    char **argv = 0;

    QApplication app(argc, argv);


    Window w(name, windowWidth+2, windowHeight+2, frameRate);

    w.resize(QSize(windowWidth+2, windowHeight+2));
    w.show();

    app.exec();
}

