#-------------------------------------------------
#
# Project created by QtCreator 2015-02-24T14:21:02
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Hwk5Sullivans
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += c++11

TEMPLATE = app


SOURCES += main.cpp
