#include "maze.h"
#include <iostream>

int generateRandom(int maxValue);  // generate a number between 0 and maxValue

Maze::Maze(int width, int height)
{
    _cells.resize(width * height);
    _width = width;
    _height = height;

}

Maze::~Maze()
{

}

void Maze::setWall(RowCol position, Direction direction, bool hasWall)
{
    switch (direction)
    {
    case Direction::Up:
        cell(position).topWall = hasWall;
        if (position.row > 0)
        {
            cell({position.row-1, position.col}).bottomWall = hasWall;
        }
        break;
    case Direction::Left:
        cell(position).leftWall = hasWall;
        if (position.col > 0)
        {
            cell({position.row, position.col-1}).rightWall = hasWall;
        }
        break;
    case Direction::Right:
        cell(position).rightWall = hasWall;
        if (position.col < _width-1)
        {
            cell({position.row, position.col+1}).leftWall = hasWall;
        }

        break;
    case Direction::Down:
        cell(position).bottomWall = hasWall;
        if (position.row < _height-1)
        {
            cell({position.row+1, position.col}).topWall = hasWall;
        }
        break;
    }
}

Cell::Cell()
{
    visited = false;
    topWall = true;
    leftWall = true;
    bottomWall = true;
    rightWall = true;
}

Cell &Maze::cell(RowCol position)
{
    int index = _width * position.row + position.col;
    return _cells[index];
}

void Maze::buildMazeRecursive(RowCol position)
{
    std::vector<Direction> possibleDirections;
    cell(position).visited = true;
    if (position.row > 0 && !cell(position.above()).visited)
    {
        possibleDirections.push_back(Direction::Up);
    }
    if (position.row < _height-1 && !cell(position.below()).visited)
    {
        possibleDirections.push_back(Direction::Down);
    }
    if (position.col > 0 && !cell(position.toLeft()).visited)
    {
        possibleDirections.push_back(Direction::Left);
    }
    if (position.col < _width-1 && !cell(position.toRight()).visited)
    {
        possibleDirections.push_back(Direction::Right);
    }
    if (possibleDirections.size() == 0)
    {
        return;
    }
    int random = generateRandom(possibleDirections.size()-1);
    Direction chosenDirection = possibleDirections[random];
    setWall(position, chosenDirection, false);
    switch (chosenDirection)
    {
    case Direction::Up:
        buildMazeRecursive(position.above());
        break;
    case Direction::Down:
        buildMazeRecursive(position.below());
        break;
    case Direction::Right:
        buildMazeRecursive(position.toRight());
        break;
    case Direction::Left:
        buildMazeRecursive(position.toLeft());
        break;
    }
    buildMazeRecursive(position);
    return;
}

bool Maze::hasWall(RowCol position, Direction direction)
{
    switch (direction)
    {
    case Direction::Up:
        return cell(position).topWall;
    case Direction::Left:
        return cell(position).leftWall;
    case Direction::Right:
        return cell(position).rightWall;
    case Direction::Down:
        return cell(position).bottomWall;
    }

    // shouldn't get here....

    return false;
}

Direction clockwise(Direction direction)
{
    switch (direction)
    {
    case Direction::Down:
        return Direction::Left;
    case Direction::Left:
        return Direction::Up;
    case Direction::Up:
        return Direction::Right;
    case Direction::Right:
        return Direction::Down;
    }
}

bool Maze::isValidMove(RowCol position, Direction direction)
{
    if ((direction == Direction::Up) && position.row == 0)
    {
        return false;
    }
    if (direction == Direction::Down && position.row == _height-1)
    {
        return false;
    }
    if (direction == Direction::Left && position.col == 0)
    {
        return false;
    }
    if (direction == Direction::Right && position.col == _width-1)
    {
        return false;
    }
    if (hasWall(position, direction))
    {
        return false;
    }
    return true;
}

Direction counterClockwise(Direction direction)
{
    switch (direction)
    {
    case Direction::Down:
        return Direction::Right;
    case Direction::Left:
        return Direction::Down;
    case Direction::Up:
        return Direction::Left;
    case Direction::Right:
        return Direction::Up;
    }
}

RowCol moveDirection(RowCol pos, Direction dir)
{
    RowCol newPos = pos;
    switch(dir)
    {
    case Direction::Up:
        newPos = pos.above();
        break;
    case Direction::Down:
        newPos = pos.below();
        break;
    case Direction::Right:
        newPos = pos.toRight();
        break;
    case Direction::Left:
        newPos = pos.toLeft();
        break;
    }
}

// moved is the direction we just moved in order to get where we are now.
bool Maze::solveRecursive(RowCol position, Direction moved, std::vector<RowCol>& path)
{
    if (position.row == _height-1 && position.col == _width-1)
    {
        path.push_back(position);
        return true;
    }

    Direction leftTurn  = counterClockwise(moved);
    Direction forward   = moved;
    Direction rightTurn = clockwise(moved);

    if (isValidMove(position, leftTurn))
    {
        if (solveRecursive(moveDirection(position,leftTurn), leftTurn, path))
        {
            path.push_back(position);
            return true;
        }
    }
    if (isValidMove(position, forward))
    {
        if (solveRecursive(moveDirection(position,forward), forward, path))
        {
            path.push_back(position);
            return true;
        }
    }
    if (isValidMove(position, rightTurn))
    {
        if (solveRecursive(moveDirection(position,rightTurn), rightTurn, path))
        {
            path.push_back(position);
            return true;
        }
    }
    return false;
}

void Maze::solveMaze(std::vector<RowCol>& path)
{
    RowCol startpos;
    startpos.row = 0;
    startpos.col = 0;
    // do some recursive stuff here....

    solveRecursive(startpos, Direction::Right, path);
}


int  Maze::width()
{
    return _width;
}

int  Maze::height()
{
    return _height;
}

bool Maze::testWalls()
{
    for (int r = 0; r < height(); r++)
    {
        for (int c = 0; c < width(); c++)
        {
            RowCol pos{r,c};

            if (c > 0 && hasWall(pos,Direction::Left) && !hasWall(pos.toLeft(),Direction::Right))
            {
                std::cout << "Cell at Row: " << r << " Col: " << c << " Has a problem with the left wall\n";
                return false;
            }
            if (c < width()-1 && hasWall(pos,Direction::Right) && !hasWall(pos.toRight(),Direction::Left))
            {
                std::cout << "Cell at Row: " << r << " Col: " << c << " Has a problem with the right wall\n";
                return false;
            }
            if (r > 0 && hasWall(pos,Direction::Up) && !hasWall(pos.above(),Direction::Down))
            {
                std::cout << "Cell at Row: " << r << " Col: " << c << " Has a problem with the top wall\n";
                return false;
            }
            if (r < height()-1 && hasWall(pos,Direction::Down) && !hasWall(pos.below(),Direction::Up))
            {
                std::cout << "Cell at Row: " << r << " Col: " << c << " Has a problem with the bottom wall\n";
                return false;
            }
        }
    }

    return true;
}

void Maze::buildMaze()
{
    RowCol position = {0,0};
    buildMazeRecursive(position);
}
