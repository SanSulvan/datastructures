#include "vec2d.h"
#include "remoteai.h"
#include <sstream>

#include "bothandler.h"
#include <qbytearray.h>

QByteArray clean(QByteArray data)
{
     for (int i=0;i<data.size();i++)
     {
         switch (data[i])
         {
         case '\n':
             data[i] = '\\';
             break;
         case '\r':
             data[i] = '/';
             break;
         }
     }

     return data;
}

RemoteAI::RemoteAI(qintptr socketId)
{
    socketDescriptor = socketId;
    socket = new QTcpSocket();



    // set the ID
    if(!socket->setSocketDescriptor(this->socketDescriptor))
    {
        qDebug() << "Error setting socket descriptor";
        return;
    }
    // connect socket and signal
    // note - Qt::DirectConnection is used because it's multithreaded
    // This makes the slot to be invoked immediately, when the signal is emitted.
    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
    connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(socketStateChanged(QAbstractSocket::SocketState)), Qt::QueuedConnection);
    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(socketError(QAbstractSocket::SocketError)), Qt::QueuedConnection);

    qDebug() << socketDescriptor << " Client connected";

    socket->setSocketOption(QAbstractSocket::LowDelayOption, 1);
    socket->write("Blocked 0 0\r\n.\r\n");
}

RemoteAI::~RemoteAI()
{
    if (socket)
    {
        socket->close();
        delete socket;
    }
}

void RemoteAI::socketStateChanged(QAbstractSocket::SocketState ss)
{
    qDebug() << "Socket State Changed: " << (int)ss;
}

void RemoteAI::socketError(QAbstractSocket::SocketError se)
{
    qDebug() << "Socket Error Signal: " << (int)se;
}

/*

Fire();
Scan(fieldOfView); // maximum field of view is given by maxFieldOfView function
Move(speed);       // maximum speed is given by maxSpeed function
Turn(radians);

*/

void RemoteAI::sendData(QByteArray data)
{
    if (socket)
    {
        socket->write(data);
        socket->flush();
        //if (!data.startsWith("Idl"))
        {
            //qDebug() << "-------> Sending: " << clean(data);
        }
    }
    else
    {
        qDebug() << "Not Sending (disconnected): " << data;
    }

}

void RemoteAI::readyRead()
{
    //qDebug() << "In readyRead: " << QThread::currentThreadId();

    // get the information
    QByteArray newdata = socket->readAll();

    {
        QMutexLocker locker(&mutex);
        data.append(newdata);
    }

    // will write on server side window
    //qDebug() << " Received Data: " << clean(newdata);
}

void RemoteAI::disconnected()
{
    std::cout << botName << " Disconnected\n";
    setDisconnected();
}

void RemoteAI::handleEvents(std::vector<BotEvent> events, double currentTime)
{
    std::stringstream ss;
    for (BotEvent& event : events)
    {
        event.write(ss);
    }

    ss << ". " << currentTime << "\r\n";

    sendData(ss.str().c_str());
}

void RemoteAI::asyncWait()
{
    handleCommand();
}

bool RemoteAI::handleCommand()
{
    std::string cmd;

    {
        QMutexLocker locker(&mutex);

        if (data.endsWith('\n'))
        {
            cmd = data.constData();
            data.clear();
        }
    }

    if (!cmd.empty())
    {
        handleCommand(cmd);
        return true;
    }

    return false;
}

void RemoteAI::handleCommand(std::string cmd)
{
    std::cout << botName << " sent command: " << cmd.c_str();

    if (cmd.size() < 1)
    {
        return;
    }

    std::stringstream ss(cmd);

    std::string cmdName;

    double arg1 = 0;
    double arg2 = 0;

    ss >> cmdName;

    switch (cmd[0])
    {
    case 'F': // fire
        Fire();
        break;
    case 'M': // move
        ss >> arg1;
        ss >> arg2;
        Move(arg1, arg2);
        break;
    case 'T': // turn
        ss >> arg1;
        Turn(arg1);
        break;
    case 'S': // scan
        ss >> arg1;
        Scan(arg1);
        break;
    case 'N':
        ss >> botName;
        break;
    default:
        return;
    }
}
