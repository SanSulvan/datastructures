#include "vec2d.h"
#include "spencerai.h"
#include <iostream>

SpencerAI::SpencerAI()
{

}

/*

Shoot();
Scan(fieldOfView); // maximum field of view is given by maxFieldOfView function
Move(speed);       // maximum speed is given by maxSpeed function
Turn(radians);

*/
int moveMod;
int nextCmd;
bool foe = false;
double nextCmdArg;

void SpencerAI::whenShotFired(double /* elapsedTime */)
{
    if (nextCmd == 3)
    {
        Turn(nextCmdArg);
    }
    else if (nextCmd == 1)
    {
        Scan(nextCmdArg);
    }
    else
    {
        moveMod = 20;
        nextCmd = 1;
        nextCmdArg = M_PI;
        Move(50);
    }
}

void SpencerAI::whenScanCompleted(double /* elapsedTime */, std::vector<int>  view)
{
    for (size_t i = 0; i < view.size(); i++)
    {
//        std::cout << view[i];
        if (view[i] != 0x000000)
        {
//            std::cout << M_PI << " " << M_PI/2 << std::endl;
//            std::cout << nextCmdArg << " " << i << " " << view.size() << " " << nextCmdArg/view.size() << std::endl;
//            std::cout << i - ((int) view.size()) / 2;
//            std::cout << "\nBot Spotted: " << (i - ((int) view.size()) / 2) * (nextCmdArg/view.size()) << "\n\n";
            foe = true;
            Turn((((int) view.size()) / 2 - i) * (nextCmdArg/view.size()) + 10 * nextCmdArg/view.size());
        }
    }

    std::cout << std::endl;

    if (!foe)
    {

        moveMod = 20;
        nextCmd = 3;
        nextCmdArg = M_PI/2;
        Move(50);
    }
}

void SpencerAI::whenTurnCompleted(double /* elapsedTime */)
{
    // nextCmd is set to -1 to prevent functions from using leftover values.
    nextCmd = -1;
    if (foe)
    {
        foe = false;
        nextCmd = 1;
        nextCmdArg = M_PI/4;
        Fire();
    }
    else
    {
        nextCmdArg = M_PI;
        Scan(nextCmdArg);
    }
}

void SpencerAI::whenMoveBlocked(double /* elapsedTime */)
{
    nextCmd = 3;
    nextCmdArg = M_PI/2;
    Fire();
}

void SpencerAI::whileIdle(double /* elapsedTime */)
{
    nextCmdArg =
            M_PI;
    Scan(nextCmdArg);
}

void SpencerAI::whileMoving(double /* elapsedTime */)
{
    static int moveModTick = 1;

    if (moveModTick++ % (moveMod + 1) == 0)
    {
        moveModTick = 1;
        switch(nextCmd)
        {
        case 0:
            Fire();
            break;
        case 1:
            Scan(nextCmdArg);
            break;
        case 2:
            Move(nextCmdArg);
            break;
        case 3:
            Turn(nextCmdArg);
        }
    }
}
