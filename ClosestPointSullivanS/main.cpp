#include "vec2d.h"
#include "graphics.h"
#include <cmath>
#include <vector>
#include <iostream>
#include <sstream>
#include "ImageDraw.h"
#include <cstdlib>
#include <ctime>
#include <algorithm>

void initializeRandomNumberGenerator() // put this in your main
{
    srand(time(0));
}

int generateRandom(int maxValue)  // generate a number between 0 and maxValue
{
    return rand()%(maxValue+1);
}

using namespace std;

Vec2d mousePos;  // set automatically to the current mouse location

class CPoint
{
public:
    Vec2d location;
    int   color;
};

std::vector<CPoint> points;
CPoint closest1;
CPoint closest2;


void updateDisplay(int /* elapsedMs */)
{
    for (auto& p : points)
    {
        drawPoint(p.location, p.color);
    }

    drawLine(closest1.location, closest2.location, 0xFFFFFF);
    // draw a line between closest1, closest2
}

bool compareX(const CPoint& p1, const CPoint& p2)
{
    return p1.location.x < p2.location.x;
}

void sortByX(std::vector<CPoint>& points)
{
    std::sort(points.begin(), points.end(), compareX);
}

bool compareY(const CPoint& p1, const CPoint& p2)
{
    return p1.location.y < p2.location.y;
}

void sortByY(std::vector<CPoint>& points)
{
    std::sort(points.begin(), points.end(), compareY);
}

double findDistance(CPoint p1, CPoint p2)
{
    return (p1.location-p2.location).magnitude();
}

void findClosest(std::vector<CPoint> points, CPoint& p1, CPoint& p2)
{
    if (points.size() == 3)
    {
        if (findDistance(points[0],points[1]) < findDistance(points[1],points[2]) && findDistance(points[0],points[1]) < findDistance(points[0],points[2]))
        {
            p1 = points[0];
            p2 = points[1];
        }
        else if (findDistance(points[2],points[1]) < findDistance(points[0],points[2]) && findDistance(points[2],points[1]) < findDistance(points[0],points[1]))
        {
            p1 = points[1];
            p2 = points[2];
        }
        else
        {
            p1 = points[0];
            p2 = points[2];
        }
    }
    else if (points.size() == 2)
    {
        p1 = points[0];
        p2 = points[1];
    }
    else
    {
        std::vector<CPoint> left;
        std::vector<CPoint> right;

        int i = 0;
        while(i < points.size())
        {
            if (i < points.size()/2)
            {
                left.push_back(points[i]);
            }
            else
            {
                right.push_back(points[i]);
            }
            i++;
        }
        CPoint leftp1;
        CPoint leftp2;
        CPoint rightp1;
        CPoint rightp2;
        findClosest(left, leftp1, leftp2);
        findClosest(right, rightp1, rightp2);

        if (findDistance(leftp1,leftp2) < findDistance(rightp1,rightp2))
        {
            p1 = leftp1;
            p2 = leftp2;
        }
        else
        {
            p1 = rightp1;
            p2 = rightp2;
        }
    }
    vector<CPoint> valuesInStrip;
    for (int i = 0; i < points.size(); i++)
    {
        CPoint centerPoint;
        centerPoint.location.x = points[points.size()/2].location.x;
        centerPoint.location.y = points[i].location.y;
        if (findDistance(points[i],centerPoint) < (findDistance(p1,p2)/2))
        {
            valuesInStrip.push_back(points[i]);
        }
    }

    sortByY(valuesInStrip);

    int j = 1;

    for (int i = 0; i < valuesInStrip.size()-1; i++)
    {
        j = i+1;
        while (j < valuesInStrip.size())
        {
            if (valuesInStrip[i].location.y-valuesInStrip[j].location.y > findDistance(p1,p2))
            {
                break;
            }
            if (findDistance(valuesInStrip[i],valuesInStrip[j]) < findDistance(p1,p2))
            {
                p1 = valuesInStrip[i];
                p2 = valuesInStrip[j];
            }
            j++;
        }
    }

    // p1 and p2 will be the minimum distance points

    // sort by y
    // filter based on distance from center point

    // loop over those points to find distances with adjacent points

    // return the best
}

void generatePoints(int count)
{
    points.clear();

    for (int i =0; i< count; i++)
    {
        CPoint p;
        p.color    = 0xFFFFFF; // generateRandom(0xFFFFFF);
        p.location.x = generateRandom(1000) - 500;
        p.location.y = generateRandom(500) - 250;

        points.push_back(p);
    }


    sortByX(points);

    findClosest(points, closest1, closest2);
    std::cout << "First point is (" << closest1.location.x << "," << closest1.location.y << ") \n";
    std::cout << "Second point is (" << closest2.location.x << "," << closest2.location.y << ") \n\n";
}

void keyboardFunc(unsigned char key, int /*x*/, int /*y*/)
{
    switch (key)
    {
    case 'g':
        generatePoints(10);
        break;
    case 'a':
        break;
    case 'd':
        break;
    case 's':
        break;
    }
}

void keyboardUpFunc(unsigned char /* key */, int /*x*/, int /*y*/)
{
}

void mouseFunc(int /*button*/, int /*state*/, int /*x*/, int /*y*/)
{
}

void motionFunc(int /*x*/, int /*y*/)
{
}

void passiveMotionFunc(int /*x*/, int /*y*/)
{
}

int main()
{
    initializeRandomNumberGenerator();


    beginGraphicsLoop("Hello World", 1000, 500, 24);
    return 0;
}
