#ifndef BOTHANDLER_H
#define BOTHANDLER_H

#define _USE_MATH_DEFINES

#include <QThread>
#include <QTcpSocket>
#include <sstream>

#include "world.h"
#include "remoteai.h"

class BotHandler : public QThread
{
    Q_OBJECT

    World&    world;
    RemoteAI* remoteAI;
    Bot*      bot;

    QByteArray data;

public:
    BotHandler(qintptr id, World& world, QObject *parent = 0);
   ~BotHandler();

    void run();

signals:
    void error(QTcpSocket::SocketError socketerror);
public slots:
    void readyRead();
    void disconnected();
    void sendData(QByteArray data);
private:
    QTcpSocket *socket;
    qintptr socketDescriptor;

};

#endif // BOTHANDLER_H
