#include "catch.hpp"
#include <vector>
#include <cstdlib>
#include <iostream>
#include <ctime>

void sort(std::vector<double> &vec)
{
    // terminating case
    if (vec.size() <= 1)
    {
        std::cout << "GOT A SIZE 1 BOYS" << std::endl;
        return;
    }

    std::cout << vec.size() << std::endl;

    std::vector<double> first;
    std::vector<double> second;

    for (int i = 0; i < (vec.size()/2); i++)
    {
        first.push_back(vec[i]);
    }

    for (int j = (vec.size()/2); j < vec.size(); j++)
    {
        second.push_back(vec[j]);
    }

    sort(first);
    sort(second);

    // gotta merge

    std::vector<double> newVec;

    int i = 0;
    int j = 0;

    while (newVec.size() < vec.size())
    {
        if (first[i] < second[j])
        {
            newVec.push_back(first[i]);
            if (i < first.size()-1)
            {
                i++;
            }
            if (i == first.size()-1)
            {
                for (int k = j; k < second.size(); k++)
                {
                    newVec.push_back(second[k]);
                }
            }
        }
        else
        {
            newVec.push_back(second[j]);
            if (j < second.size()-1)
            {
                j++;
            }
            if (j == second.size()-1)
            {
                for (int p = i; p < second.size(); p++)
                {
                    newVec.push_back(first[p]);
                }
            }
        }
    }

    for (int i = 0; i < vec.size(); i++)
    {
        vec[i] = newVec[i];
    }
}
