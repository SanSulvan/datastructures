#ifndef WORLD_H
#define WORLD_H

#include <vector>
#include <set>

#include "bot.h"
#include "vec2d.h"

#include <QMutex>
#include <QMutexLocker>
#include "cameraview.h"

class World
{
    std::vector<Bot*>   bots;
    std::vector<Bullet> bullets;
    std::vector<int>    lastView;
    std::vector<Bot*>   deadBots;

    double fieldTop;
    double fieldBottom;
    double fieldLeft;
    double fieldRight;

    QMutex mutex;

    int nextBotColor;

public:
    World(double fieldTop, double fieldBottom, double fieldLeft, double fieldRight);
   ~World();
    void draw(double elapsedTime);
    void addRandomBots(int count);
    void addBot(Bot* bot);
    bool collidesWithAnyBot(Bot &bot, double margin = 0.0);
    CameraView look(double fieldOfView, Vec2d cameraPos, double cameraDirection, int resolution = 500);

    int availableBotId();

    void fire(Vec2d bulletPos, double bulletDirection, double bulletOffset);
    void update(double gameTime, double elapsedTime);

    int hitWall(Vec2d position, double radius, double& wallNormalAngle);  // 0 = no collision bits 1-4 = top,right,bottom,left
};

#endif // WORLD_H
