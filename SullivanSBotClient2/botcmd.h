#ifndef BOTCMD
#define BOTCMD

#include <iostream>
#include "cameraview.h"

class Bot;

enum class BotCmdType
{
    NoCommand,
    Turn,
    Move,
    Fire,
    Look,
    Disconnected
};

class BotCmd
{
protected:
    double totalElapsedTime;
    double lastApplyTime;
public:
    BotCmd();
    virtual ~BotCmd() {}
    virtual void apply(Bot* bot, double currentTime, double elapsedTime);
    virtual bool isComplete()    = 0;
    virtual bool isInterruptable()      = 0;
    virtual void getCmdColor(Bot* bot, int& circleColor, int& bodyColor) = 0;
    virtual bool nextStateIsNull() { return false; }
    virtual std::string name() = 0;
};

enum class BotEventType
{
    TurnComplete,
    MoveComplete,
    ShotFired,
    MoveBlocked,
    ScanComplete,
    BotCollision,
    WallCollision,
    BulletCollision,
};

class BotEvent
{
public:
    BotEventType eventType;
    double       eventTime;
    double       collisionAngle;  // angle with respect to the direction the bot is facing
    double       elapsedTime;
    double       travelDistance;
    CameraView   view;
public:
    BotEvent(BotEventType eventType, double eventTime, double collisionAngle, double travelDistance, const CameraView& cv = CameraView());

    void write(std::ostream& strm);
    static BotEvent read(std::istream& strm);
};

#endif // BOTCMD

