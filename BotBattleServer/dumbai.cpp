#include "vec2d.h"
#include "dumbai.h"

int generateRandom(int maxValue);
double randomValue(double minValue, double maxValue);

DumbAI::DumbAI()
{

}

void DumbAI::chooseRandomCmd()
{
    switch (generateRandom(2))
    {
    case 0:
        Fire();
        break;
    case 1:
        Move(randomValue(20,50));
        break;
    case 2:
        Turn(randomValue(-M_PI, M_PI));
        break;
    }
}

void DumbAI::whenShotFired(double /* elapsedTime */)
{
    chooseRandomCmd();
}

void DumbAI::whenScanCompleted(double /* elapsedTime */, std::vector<int> /* view */)
{
    chooseRandomCmd();
}

void DumbAI::whenTurnCompleted(double /* elapsedTime */)
{
    chooseRandomCmd();
}

void DumbAI::whenMoveBlocked(double /* elapsedTime */)
{
    chooseRandomCmd();
}

void DumbAI::whileIdle(double /* elapsedTime */)
{
    chooseRandomCmd();
}

void DumbAI::whileMoving(double /* elapsedTime */)
{

}
