TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

CONFIG += c++11

SOURCES += main.cpp \
    tests.cpp \
    mystack.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    catch.hpp \
    mystack.h

