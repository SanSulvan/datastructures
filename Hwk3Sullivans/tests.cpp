#include "catch.hpp"
#include <vector>
#include <cstdlib>
#include <iostream>
#include <ctime>
#include "sortfunction.cpp"

TEST_CASE( "some tests for sort function", "[vector]" ) {

    std::vector<double> MyVector;

    std::srand(std::time(0));

    for (int i = 0; i < 50; i++)
    {
        MyVector.push_back(std::rand() % 50);
        std::cout << MyVector[i];
    }

    std::vector<double> newVector = sort(MyVector);

    SECTION( "testing correct order" )
    {
        for (int i = 0; i < 49; i++)
        {
            REQUIRE ( newVector[i] <= newVector[i+1] );
        }
    }

    SECTION( "making sure elements are still there")
    {
        for (int i = 0; i < 50; i++)
        {
            bool inNew = false;
            for (int j = 0; j < 50; j++)
            {
                if (MyVector[i] == newVector[j])
                {
                    inNew = true;
                }
            }
            REQUIRE ( inNew == true );
        }
    }
}
