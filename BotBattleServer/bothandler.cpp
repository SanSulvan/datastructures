#include "bothandler.h"


BotHandler::BotHandler(qintptr ID, World& w, QObject *parent) : QThread(parent), world(w)
{
    this->socketDescriptor = ID;
}

BotHandler::~BotHandler()
{

}

void BotHandler::run()
{

}

void BotHandler::readyRead()
{

}

void BotHandler::disconnected()
{
    qDebug() << socketDescriptor << " Disconnected";
    socket->deleteLater();
    exit(0);
}

void BotHandler::sendData(QByteArray data)
{
    qDebug() << "In sendData: " << QThread::currentThreadId();

    socket->write(data);
}
