#include "botai.h"


BotAI::BotAI()
{
    botName = "DumbBot";
    disconnected = false;
    setCmd(BotCmdType::Idle, 0, 0);
}

void BotAI::setDisconnected()
{
    QMutexLocker locker(&mutex);
    disconnected = true;
}

bool BotAI::isDisconnected()
{
    QMutexLocker locker(&mutex);
    return disconnected;
}

void BotAI::setCmd(BotCmdType cmd, double arg1, double arg2)
{
    QMutexLocker locker(&mutex);

    nextCommand     = cmd;
    nextCommandArg1 = arg1;
    nextCommandArg2 = arg2;
}

void BotAI::getCmd(BotCmdType& cmd, double& arg1, double& arg2)
{
    QMutexLocker locker(&mutex);

    if (disconnected)
    {
        cmd  = BotCmdType::Disconnected;
        arg1 = 0;
        arg2 = 0;
    }
    else
    {
        cmd = nextCommand;
        arg1 = nextCommandArg1;
        arg2 = nextCommandArg2;
    }
}

void BotAI::handleEvents(std::vector<BotEvent> /*events*/, double /*currentTime*/)
{

}

void BotAI::asyncWait()
{

}

void BotAI::Turn(double angle)
{
    setCmd(BotCmdType::Turn, angle, 0);
}

void BotAI::Move(double speed, double time)
{
   if (speed < -50)
   {
       speed = -50;
   }
   if (speed > 50)
   {
       speed = 50;
   }
   setCmd(BotCmdType::Move, speed, time);
}

void BotAI::Fire()
{
   setCmd(BotCmdType::Fire, 2, 0);
}

void BotAI::Scan(double fieldOfView)
{
    if (fieldOfView < MY_PI/180)
    {
        fieldOfView = MY_PI/180;
    }
    if (fieldOfView > MY_PI)
    {
        fieldOfView = MY_PI;
    }
    setCmd(BotCmdType::Look, fieldOfView, 0);
}

void BotAI::ResetCmd()
{
    setCmd(BotCmdType::NoCommand, 0, 0);
}


