#include "myqueue.h"

//double values[100];
//int    top;

MyQueue::MyQueue()
{
    currentsize = 0;
    size = 10;
    values = new double[size];
    top = -1;
    front = 0;
}

MyQueue::~MyQueue()
{
    delete [] values;
}

void MyQueue::pushBack(double value)
{
    if (currentsize < size)
    {
        if (top < size-1)
        {
            values[++top] = value;
        }
        else
        {
            top = -1;
            values[++top] = value;
        }
        currentsize++;
    }
    else
    {
        int oldsize = size;
        size += 10;

        double *newValues = new double[size];

        for (auto i=front; i<oldsize; i++)
        {
            newValues[i-front] = values[i];
        }
        for (auto j=0; j<front; j++)
        {
            newValues[oldsize-front+j] = values[j];
        }

        newValues[oldsize] = value;

        currentsize++;
        front = 0;
        top = oldsize;

        delete [] values;

        values = newValues;
    }
}

double MyQueue::popFront()
{
    int x = values[front];
    if (front < size - 1)
    {
        front++;
    }
    else
    {
        front = 0;
    }
    currentsize--;
    return x;
}

double MyQueue::frontQueue()
{
    return values[front];
}

bool MyQueue::isEmpty()
{
    return currentsize == 0;
}

double MyQueue::sizeQueue()
{
    return currentsize;
}

double MyQueue::valueInQueue(int index)
{
    return values[index];
}
