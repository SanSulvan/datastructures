#-------------------------------------------------
#
# Project created by QtCreator 2015-04-10T14:01:44
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = PriorityQueueSullivanS
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += c++11

TEMPLATE = app


SOURCES += main.cpp \
    priorityqueue.cpp

HEADERS += \
    priorityqueue.h
