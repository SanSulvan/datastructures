TEMPLATE = app

QT += opengl widgets

CONFIG += console

QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp \
    graphics.cpp \
    font.cpp \
    vec2d.cpp \
    ImageDraw.cpp \
    maze.cpp

HEADERS += \
    graphics.h \
    font.h \
    vec2d.h \
    internal.h \
    ImageDraw.h \
    maze.h

OTHER_FILES +=

