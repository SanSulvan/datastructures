#include "vec2d.h"
#include "AISullivanS.h"
#include <iostream>

SullivanSAI::SullivanSAI()
{
    speed = 0;
    enemySpotted = -1;
    readyToFire = false;
    noFire = false;
    scanAngle = M_PI/2;
    turnRight = false;
}

/*

Shoot();
Scan(fieldOfView); // maximum field of view is given by maxFieldOfView function
Move(speed);       // maximum speed is given by maxSpeed function
Turn(radians);

*/

void SullivanSAI::whenShotFired(double /* elapsedTime */)
{
    noFire = true;
    if (turnRight)
    {
        Turn(M_PI/4);
    }
    else
    {
        Turn(-M_PI/4);
    }
}

void SullivanSAI::whenScanCompleted(double /* elapsedTime */, std::vector<int> view)
{
    lastCamera = view;
    enemySpotted = -1;
    for (int i = 0; i < lastCamera.size(); i++)
    {
        if (lastCamera[i] != 0 && enemySpotted == -1)
        {
            enemySpotted = i;
            std::cout << "\ntest\n";
        }
    }
    double turnAngle;
    std::cout <<  "\n" << enemySpotted << "\n";

    if (enemySpotted >= 0)
    {
        turnAngle = (enemySpotted*scanAngle/lastCamera.size())-(scanAngle/2)+(M_PI*2/180);
        readyToFire = true;
        std::cout << "\nturnangle = " << turnAngle << "\n";
    }
    else
    {
        readyToFire = false;
        turnAngle = M_PI/2;
        std::cout << "\ntest3\n";
    }

    if (turnAngle < 0)
    {
        turnRight = true;
    }
    else
    {
        turnRight = false;
    }

    Turn(-turnAngle);
}

void SullivanSAI::whenTurnCompleted(double /* elapsedTime */)
{
    if (readyToFire)
    {
        readyToFire = false;
        Fire();
    }
    else if (noFire)
    {
        noFire = false;
        speed = 50;
        Move(speed);
    }
    else
    {
        Scan(scanAngle);
    }

}

void SullivanSAI::whenMoveBlocked(double /* elapsedTime */)
{
    Scan(scanAngle);
}

void SullivanSAI::whileIdle(double /* elapsedTime */)
{
    speed = -50;
    Move(speed);
}

void SullivanSAI::whileMoving(double /* elapsedTime */)
{
    if (speed == -50)
    {
        speed = -45;
        Move(speed);
    }
    else if (speed == -45)
    {
        speed = -50;
        Move(speed);
    }
    else if (speed == 50)
    {
        speed = 45;
        Move(speed);
    }
    else if (speed == 45)
    {
        speed = 50;
        Move(speed);
    }
}
