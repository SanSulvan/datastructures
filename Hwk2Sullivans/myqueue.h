#ifndef MYQUEUE_H
#define MYQUEUE_H


class MyQueue
{
private:
    int currentsize; // current number of values
    int     size; // size of the values array
    double *values;
    int     top; // index of the last value
    int     front; // index of the first value
public:
    MyQueue();
    ~MyQueue();
    void pushBack(double value);
    double valueInQueue(int index);
    double popFront();
    double frontQueue();
    double sizeQueue();
    bool isEmpty();
};

#endif // MYQueue_H
