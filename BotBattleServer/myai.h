#ifndef MYAI_H
#define MYAI_H

#include "botai.h"

class MyAI : public BotAI
{
public:
    MyAI();

    void handleEvents(std::vector<BotEvent> events, double currentTime);
};

#endif // MYAI_H
