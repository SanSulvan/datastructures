#include <Sparki.h>

void setup()
{
  sparki.moveForward();
}

void loop()
{
  if (sparki.lineRight() < 500)
  {
    sparki.RGB(RGB_BLUE);
    sparki.moveForward(.2);
    sparki.moveLeft(90);
    sparki.moveBackward(2);
  }
  else if (sparki.edgeRight() < 500)
  {
    sparki.RGB(RGB_GREEN);
    sparki.moveForward();
  }
  else
  {
    sparki.RGB(RGB_RED);
    sparki.moveStop();
    sparki.moveForward(7.7);
    sparki.moveRight(90);
    sparki.moveForward(.4);
  }
}
