#ifndef DUMBAI_H
#define DUMBAI_H

#include "botai.h"

class DumbAI : public BotAI
{
public:
    DumbAI();

    void chooseRandomCmd();

    void whenShotFired(double elapsedTime);
    void whenScanCompleted(double elapsedTime, std::vector<int> view);
    void whenTurnCompleted(double elapsedTime);
    void whenMoveBlocked(double elapsedTime);
    void whileIdle(double elapsedTime);
    void whileMoving(double elapsedTime);
};

#endif // DUMBAI_H
