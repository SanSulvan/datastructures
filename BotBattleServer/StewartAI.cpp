#include "vec2d.h"
#include "StewartAI.h"
#include "iostream"



StewartAI::StewartAI()
{

}

/*

F();
Scan(fieldOfView); // maximum field of view is given by maxFieldOfView function
Move(speed);       // maximum speed is given by maxSpeed function
Turn(radians);

*/

void StewartAI::whenShotFired(double /* elapsedTime */)
{
    Scan(M_PI/2);
}

void StewartAI::whenScanCompleted(double /* elapsedTime */, std::vector<int> view )
{
    if (view[view.size()/2])
    {
        Fire();
        return;
    }

    for (unsigned int i = 0; i < view.size()/2; i++)
    {
        if (view[i])
        {
            //std::cout << "left: " << (M_PI/4)*(((view.size()/2)-i)/(view.size()/2)) << "/n";
            //Turn((M_PI/4)*(((view.size()/2)-i)/(view.size()/2)));
            Turn(.1);
            return;
        }
    }

    for (unsigned int i = view.size()/2; i < view.size(); i++)
    {
        if (view[i])
        {
            //std::cout << "right: " << (-(M_PI/4)*((i-(view.size()/2))/(view.size()/2))) << "/n";
            //Turn(-(M_PI/4)*((i-(view.size()/2))/(view.size()/2)));
            Turn(-.1);
            return;
        }
    }

    Turn(2*M_PI/3);
}

void StewartAI::whenTurnCompleted(double /* elapsedTime */)
{
    Scan(M_PI/2);
}

void StewartAI::whenMoveBlocked(double /* elapsedTime */)
{
    Turn(M_PI/4);
}

void StewartAI::whileIdle(double /* elapsedTime */)
{
    Scan(M_PI/2);
}

void StewartAI::whileMoving(double /* elapsedTime */)
{
    Turn(.1);
}
