#include "botserver.h"
#include "bothandler.h"

#include <QMetaType>

BotServer::BotServer(World& w, QObject *parent) : QTcpServer(parent), world(w)
{
    qRegisterMetaType< QAbstractSocket::SocketState >();
    qRegisterMetaType< QAbstractSocket::SocketError >();
}

BotServer::~BotServer()
{

}

void BotServer::startServer()
{
    int port = 1234;
    if(!this->listen(QHostAddress::Any, port))
    {
        qDebug() << "Could not start server";
    }
    else
    {
        qDebug() << "Listening to port " << port << "...";
    }
}

// This function is called by QTcpServer when a new connection is available.

void BotServer::incomingConnection(qintptr socketDescriptor)
{
    // We have a new connection
    qDebug() << "Bot Connecting";

    auto remoteAI = new RemoteAI(socketDescriptor);

    if (world.availableBotId() >= 10)
    {
        qDebug() << "Bot Limit Reached... refusing";
        delete remoteAI;
    }
    else
    {
        auto bot = new Bot(&world, remoteAI);
        world.addBot(bot);
    }
}
