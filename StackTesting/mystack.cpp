#include "mystack.h"

//double values[100];
//int    top;

MyStack::MyStack()
{
    size = 10;
    values = new double[size];
    top = -1;
}

MyStack::~MyStack()
{
    delete [] values;
}

void MyStack::push(double value)
{
    if (top < (size-1))
    {
        values[++top] = value;
    }
    else
    {
        size += 10;

        double *newValues = new double[size];

        for (int i=0; i<=top; i++)
        {
            newValues[i] = values[i];
        }

        newValues[++top] = value;

        delete [] values;

        values = newValues;
    }
}

double MyStack::pop()
{
    return values[top--];
}

double MyStack::peek()
{
    return values[top];
}

bool MyStack::isEmpty()
{
    return top < 0;
}

