#include "catch.hpp"
#include <vector>
#include <cstdlib>
#include <iostream>
#include <ctime>
#include "tree.cpp"

TEST_CASE( "tree tests???", "[vector]" ) {

    // These aren't catch.hpp tests. However, they do show expected behavior in the function!
    Tree testTree;
    testTree.insert(3);
    testTree.insert(7);
    testTree.insert(2);
    testTree.insert(5);

    vector<double> ordered;
    testTree.inOrder(ordered);

    for (int i = 0; i < 4; i++)
    {
        cout << "\nThe " << i+1 << "th value is " << ordered[i] << endl;
    }

    testTree.deleteValue(4);
    testTree.deleteValue(7);
    testTree.deleteValue(5);
    testTree.deleteValue(2);

    cout << endl << testTree.root->value << endl;

    vector<double> inOrderVec;
    testTree.inOrder(inOrderVec);

    cout << "\nThe last value is " << inOrderVec[0] << endl;

    testTree.deleteValue(3);
    cout << endl << testTree.root << endl;
    testTree.deleteValue(8);
    //END OF NOT ACTUAL TESTS

}
