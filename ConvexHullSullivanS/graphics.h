#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <string>
#include <vector>

#include "vec2d.h"

void drawLine(Vec2d p1, Vec2d p2, unsigned int color);
void drawPoint(Vec2d Vec2d, unsigned int color);
void drawString(Vec2d position, std::string text, double scale, unsigned int color);

void drawPolyline(const std::vector<Vec2d>& pts, unsigned int color);
void drawPolyline(std::vector<Vec2d> pts, Vec2d offset, double scale, double angle, unsigned int color);

void drawPolygon(const std::vector<Vec2d>& pts, unsigned int color);
void drawPolygon(std::vector<Vec2d> pts, Vec2d offset, double scale, double angle, unsigned int color);

void fillPolygon(const std::vector<Vec2d>& pts, unsigned int color);
void fillPolygon(std::vector<Vec2d> pts, Vec2d offset, double scale, double angle, unsigned int color);

void drawEllipse(Vec2d center, double width, double height, double angle, unsigned int color);
void fillEllipse(Vec2d center, double width, double height, double angle, unsigned int color);

void beginGraphicsLoop(const std::string &windowName, int windowWidth, int windowHeight, int frameRate);

void keyboardFunc(unsigned char key, int x, int y);
void keyboardUpFunc(unsigned char key, int x, int y);
void mouseFunc(int button, int state, int x, int y);
void motionFunc(int x, int y);
void passiveMotionFunc(int x, int y);
void updateDisplay(int elapsedMs);




#endif // GRAPHICS_H
