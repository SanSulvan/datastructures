#include "catch.hpp"
#include "myqueue.h"
#include "iostream"

TEST_CASE( "some tests of my queue", "[vector]" ) {

    MyQueue queue;

    CHECK( queue.isEmpty() == true );

    SECTION( "pushing items" ) {

        queue.pushBack(37);

        CHECK( queue.frontQueue() == 37 );

        CHECK( queue.sizeQueue() == 1);

        REQUIRE( queue.isEmpty() == false );
    }

    SECTION( "pushing and popping item" ) {

        queue.pushBack(37);

        auto value = queue.popFront();

        REQUIRE( value == 37 );

        REQUIRE (queue.isEmpty() == true);
    }

    SECTION( "pushing and popping multiple items" ) {

        for (int i=0;i<50;i++)
        {
            queue.pushBack(i);

            REQUIRE( queue.sizeQueue() == i+1);
            REQUIRE( queue.valueInQueue(i) == i);
            REQUIRE( queue.frontQueue() == 0);
        }

        for (int i=0;i < 49;i++)
        {
            auto value = queue.popFront();

            REQUIRE(value == i);
            REQUIRE(queue.frontQueue() == i+1);
            REQUIRE(queue.sizeQueue() == 49-i);
        }

        queue.popFront();


        REQUIRE (queue.isEmpty() == true);
    }

    SECTION( "Dr. Hamlin's Tests")
        {
             int i=0;

             for (;i<500 ;i++ )
             {
                 queue.pushBack(i);
                 queue.pushBack(-i);

                 auto value1 = queue.frontQueue();
                 auto value2 = queue.popFront();

                 REQUIRE( value1 == value2 );

                 REQUIRE( value1 == ((i%2) ? -1 : 1) * (i/2));

                 REQUIRE( queue.sizeQueue() == i+1);
             }

             while (queue.sizeQueue() > 0)
             {
                 auto value1 = queue.frontQueue();
                 auto value2 = queue.popFront();

                 REQUIRE( value1 == value2 );

                 REQUIRE( value1 == ((i%2) ? -1 : 1) * (i/2));

                 i++;
             }

             REQUIRE (queue.isEmpty() == true);
        }
}
