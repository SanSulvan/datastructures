#ifndef AISULLIVANS_H
#define AISULLIVANS_H

#include "botai.h"

class SullivanSAI : public BotAI
{
public:
    SullivanSAI();

    void whenShotFired(double elapsedTime);
    void whenScanCompleted(double elapsedTime, std::vector<int> view);
    void whenTurnCompleted(double elapsedTime);
    void whenMoveBlocked(double elapsedTime);
    void whileIdle(double elapsedTime);
    void whileMoving(double elapsedTime);

    double speed;
    std::vector<int> lastCamera;
    double enemySpotted;
    bool readyToFire;
    bool noFire;
    double scanAngle;
    bool turnRight;
};

#endif // AISULLIVANS_H
