#include "ImageDraw.h"
#include "internal.h"


unsigned int createTexture();
void drawTexture(unsigned int texture_id, double x, double y, double z, double w, double h, int imageWidth, int imageHeight, RGBPixel *rawPixels);
RGBPixel *loadImage(const char *name, int *width, int *height);

//    QImage       _img;
//    unsigned int _texture_id;

Image::Image(int width, int height)
{
    _img = new QImage(width, height, QImage::Format_RGBA8888);
    _img->fill(Qt::green);
    *_img = QGLWidget::convertToGLFormat(*_img);
    _texture_id = -1;
}

Image::Image(const std::string &filename)
{
    _img = new QImage();

    load(filename);

    _texture_id = 0;
}

Image::~Image()
{
    delete _img;
}

RGBPixel &Image::pixel(int x, int y)
{
    return *(reinterpret_cast<RGBPixel*>(_img->scanLine(y)) + x);
}

int Image::width()
{
    return _img->width();
}

int Image::height()
{
    return _img->height();
}

void Image::load(const std::string &filename)
{
    if ( !_img->load(filename.c_str()) )
    {
       *_img = QImage( 16, 16, QImage::Format_RGB888);
       _img->fill(Qt::red);
    }

    QImage fixedImage(_img->width(), _img->height(), QImage::Format_ARGB32);
    QPainter painter(&fixedImage);
    painter.setCompositionMode(QPainter::CompositionMode_Source);
    painter.fillRect(fixedImage.rect(), Qt::transparent);
    painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
    painter.drawImage( 0, 0, *_img);
    painter.end();

    *_img = QGLWidget::convertToGLFormat(fixedImage);
}

void Image::save(const std::string &filename)
{
    QImage tmp(_img->width(), _img->height(), QImage::Format_ARGB32);

    int h = tmp.height();

    for (int y = 0; y < tmp.height(); y++)
    {
        RGBPixel *src = reinterpret_cast<RGBPixel*>(_img->scanLine(y));
        RGBPixel *dst = reinterpret_cast<RGBPixel*>(tmp.scanLine(h-y-1));

        for (int x = 0; x < tmp.width(); x++)
        {
            dst->r = src->b;
            dst->g = src->g;
            dst->b = src->r;
            dst->a = src->a;
            dst++;
            src++;
        }
    }

    tmp.save(filename.c_str());
}

void Image::draw(Vec2d position, double z, double w, double h)
{
    if (_texture_id == 0)
    {
        _texture_id = createTexture();
    }

    drawTexture(_texture_id, position.x, position.y, z, w, h, _img->width(), _img->height(), reinterpret_cast<RGBPixel*>(_img->bits()));
}

GLuint createTexture()
{
	GLuint texture_id;
	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	return texture_id;
}

void drawTexture(GLuint texture_id, double x, double y, double z, double w, double h, int imageWidth, int imageHeight, RGBPixel *rawPixels)
{
	glBindTexture(GL_TEXTURE_2D, texture_id);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imageWidth, imageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, rawPixels);
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND); // for alpha
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor3ub(255, 255, 255);
	glBegin(GL_QUADS);
    glTexCoord2f(0.0, 0.0);
    glVertex3d(x, y, z);
    glTexCoord2f(1.0, 0.0);
    glVertex3d(x+w, y, z);
    glTexCoord2f(1.0, 1.0);
    glVertex3d(x+w, y+h, z);
    glTexCoord2f(0.0, 1.0);
    glVertex3d(x, y+h, z);
	glEnd();
    glDisable(GL_BLEND);
    glDisable(GL_TEXTURE_2D);
}


