#include <limits>
#include "graphics.h"
#include "world.h"
#include <algorithm>


extern Vec2d mousePos;  // set automatically to the current mouse location
extern double cameraAngle;

using namespace std;

std::vector<unsigned int> distinctColors =
{
    // https://eleanormaclure.files.wordpress.com/2011/03/colour-coding.pdf

        0xFFB300, // Vivid Yellow
        0x803E75, // Strong Purple
        0xFF6800, // Vivid Orange
        0xA6BDD7, // Very Light Blue
        0xC10020, // Vivid Red
        0xCEA262, // Grayish Yellow
        0x817066, // Medium Gray

        // The following don't work well for people with defective color vision
        0x007D34, // Vivid Green
        0xF6768E, // Strong Purplish Pink
        0x00538A, // Strong Blue
        0xFF7A5C, // Strong Yellowish Pink
        0x53377A, // Strong Violet
        0xFF8E00, // Vivid Orange Yellow
        0xB32851, // Strong Purplish Red
        0xF4C800, // Vivid Greenish Yellow
        0x7F180D, // Strong Reddish Brown
        0x93AA00, // Vivid Yellowish Green
        0x593315, // Deep Yellowish Brown
        0xF13A13, // Vivid Reddish Orange
        0x232C16, // Dark Olive Green

};

int generateRandom(int maxValue);

Vec2d randomPosition(double minx, double maxx, double miny, double maxy)
{
    double x = minx + generateRandom(maxx - minx);
    double y = miny + generateRandom(maxy - miny);

    return Vec2d { x, y };
}

Vec2d randomPosition(double minx, double maxx, double miny, double maxy, double marginSize)
{
    return randomPosition(minx + marginSize, maxx - marginSize, miny + marginSize, maxy - marginSize);
}

double randomValue(double minValue, double maxValue)
{
    double p = (double)generateRandom(RAND_MAX-1) / RAND_MAX;
    double v = minValue + p * (maxValue - minValue);
    if (v <= minValue)
    {
        v = minValue;
    }
    if (v >= maxValue)
    {
        v = maxValue;
    }
    return v;
}

double subtractAngles(double a1, double a2)
{
    double diff = a1 - a2;

    if (diff < -MY_PI)
    {
        diff += 2*MY_PI;
    }

    if (diff > MY_PI)
    {
        diff -= 2*MY_PI;
    }

    return diff;
}

double negateAngle(double angle)
{
    angle += MY_PI;
    if (angle > 2*MY_PI)
    {
        angle -= 2*MY_PI;
    }
    return angle;
}


World::World(double fieldTop, double fieldBottom, double fieldLeft, double fieldRight)
{
    this->fieldTop    = fieldTop;
    this->fieldBottom = fieldBottom;
    this->fieldLeft   = fieldLeft;
    this->fieldRight  = fieldRight;

    nextBotColor = 0;
}

World::~World()
{
    for (auto b : bots)
    {
        delete b;
    }
    bots.clear();
}

//  0 = no collision bits 1-4 = top,right,bottom,left
int World::hitWall(Vec2d position, double radius, double& wallNormalAngle)
{
    int result = 0;

    if (position.y + radius >= fieldTop)
    {
        wallNormalAngle = 3.0*MY_PI/2;
        result |= 0x01;
    }

    if (position.y - radius <= fieldBottom)
    {
        wallNormalAngle = MY_PI/2;
        result |= 0x04;
    }

    if (position.x + radius >= fieldRight)
    {
        wallNormalAngle = MY_PI;
        result |= 0x02;
    }

    if (position.x - radius <= fieldLeft)
    {
        wallNormalAngle = 0;
        result |= 0x08;
    }

    return result;
}


void World::update(double gameTime, double elapsedTime)
{
    // move bots

    for (auto& b : bullets)
    {
        b.update(elapsedTime);

        double wallNormal;
        if (hitWall(b.position, 0, wallNormal))
        {
            b.collided = true;
        }
    }

    for (auto b : bots)
    {
        b->collided = false;
    }

    for (auto b : bots)
    {
        b->update(*this, gameTime, elapsedTime);
    }

    bots.erase(remove_if(bots.begin(), bots.end(), [](Bot* b)
               {
                   if (b->isDisconnected())
                   {
                       delete b;
                       return true;
                   }
                   return false;
               }), bots.end());

    for (auto b : bots)
    {
        double wallNormalAngle;

        if (hitWall(b->position, b->radius(), wallNormalAngle))
        {
            b->hitWall(gameTime, wallNormalAngle);
        }

        for (auto& bullet : bullets)
        {
            if (bullet.collidesWith(*b))
            {
                bullet.collided = true;
                b->gotShot(gameTime, bullet.angle);
            }
        }
    }

    if (bots.size() > 0)
    {
        for (unsigned int i=0;i<bots.size()-1;i++)
        {
            for (unsigned int j=i+1; j < bots.size(); j++)
            {
                if (bots[i]->collidesWith(*bots[j]))
                {
                    auto botDirVector = bots[i]->getPos() - bots[j]->getPos();
                    // angle from j to i
                    double angleJI = atan2(botDirVector.y, botDirVector.x);
                    double angleIJ = atan2(-botDirVector.y, -botDirVector.x);

                    bots[j]->hitBot(gameTime, angleJI);
                    bots[i]->hitBot(gameTime, angleIJ);
                }
            }
        }
    }

    for (auto b : bots)
    {
        if (b->collided && b->command->isInterruptable())
        {
            b->gotStopped(gameTime);
        }
    }

    // remove collided bullets
    bullets.erase( remove_if(bullets.begin(), bullets.end(), [](Bullet b) { return b.collided; }), bullets.end());

    bool someoneDied = false;
    bool someoneDisconnected = false;

    for (auto b : bots)
    {
        if (b->isDead())
        {
            someoneDied = true;
            deadBots.push_back(b);
        }

        if (b->isDisconnected())
        {
            someoneDisconnected = true;
        }
    }

    if (someoneDied || someoneDisconnected)
    {
        bots.erase(remove_if(bots.begin(), bots.end(), [](Bot* b) { return b->isDead() || b->isDisconnected(); }), bots.end());
    }

    // process events on remaining bots

    for (auto b : bots)
    {
        b->processEvents(gameTime);
    }

    for (Bot* b : deadBots)
    {
        if (b->shotEffect == 0)
        {
            b->disconnected = true;
        }
    }

    deadBots.erase(remove_if(deadBots.begin(), deadBots.end(), [](Bot* b)
               {
                   if (b->isDisconnected())
                   {
                       delete b;
                       return true;
                   }
                   return false;
               }), deadBots.end());
}

int World::availableBotId()
{
    for (size_t i=1;i<=bots.size();i++)
    {
        bool found = false;

        for (auto b : bots)
        {
            if (b->botId == (int)i)
            {
                found = true;
            }
        }

        if (found)
        {
            continue;
        }

        return i;
    }

    return bots.size()+1;
}


CameraView World::look(double fieldOfView, Vec2d cameraPosition, double cameraAngle, int resolution)
{
    CameraView view;

    view.color.resize(resolution, 0);
    view.distance.resize(resolution, std::numeric_limits<double>::max());

    for (auto b : bots)
    {
        auto viewVector = b->getPos() - cameraPosition;

        double botAngle = atan2(viewVector.y, viewVector.x);

        // atan2 returns +- MY_PI

        if (botAngle < 0) botAngle += 2 * MY_PI;

        // now botAngle is in range 0 - 2*MY_PI, which is same as cameraAngle

        double botDist = viewVector.magnitude();

        double relativeBotAngle = subtractAngles(botAngle, cameraAngle);


        //cout << "RelativeBotAngle: " << relativeBotAngle << endl;

        double angularRadius = asin(b->radius()/botDist);

        double botMaxAngle = relativeBotAngle + angularRadius;
        double botMinAngle = relativeBotAngle - angularRadius;

        if ((botMaxAngle > MY_PI) || (botMinAngle < -MY_PI))
        {
            // cout << "Behind!\n";
            continue;
        }

        double scale = (double)resolution / fieldOfView;

        double botMinPixel = resolution/2 + scale * botMinAngle;
        double botMaxPixel = resolution/2 + scale * botMaxAngle;

        if (botMinPixel < 0)
        {
            botMinPixel = 0;
        }

        if (botMaxPixel >= resolution)
        {
            botMaxPixel = resolution-1;
        }

        if (botMaxPixel < 0 || botMinPixel >= resolution)
        {
            continue;
        }

        int id = b->botId;

        for (int i = botMinPixel; i <= botMaxPixel; i++)
        {
            int idx = resolution-1-i;

            if (botDist < view.distance[idx])
            {
                view.distance[idx] = botDist;
                view.color[idx] = id;
            }
        }
    }

    lastView = view.color;

    return view;
}


void World::draw(double elapsedTime)
{
//    double fieldOfView = MY_PI/2;

    drawRectangle({fieldLeft, fieldTop},{fieldRight, fieldBottom}, 0xFFFFFF);

    for (auto b : bots)
    {
        b->draw(elapsedTime);
    }

    for (auto b : deadBots)
    {
        b->draw(elapsedTime);
    }

    for (Bullet& b : bullets)
    {
        b.draw();
    }

    if (lastView.size() > 0)
    {
        double PixelWidth = (fieldRight - fieldLeft)/lastView.size();

        for (unsigned int i=0;i<lastView.size();i++)
        {
            fillRectangle({fieldLeft + i*PixelWidth, fieldBottom - 20}, {fieldLeft + i*PixelWidth+PixelWidth, fieldBottom}, lastView[i]);
        }
    }
}

bool World::collidesWithAnyBot(Bot &newBot, double margin)
{
    for (auto* oldBot : bots)
    {
        if (oldBot->collidesWith(newBot, margin))
        {
            return true;
        }
    }

    return false;
}

void World::fire(Vec2d bulletPos, double bulletDirection, double startingOffset)
{
    Vec2d bulletVelocity { 100, 0 };
    bulletVelocity.rotate(bulletDirection);
    Vec2d bulletOffset { startingOffset, 0 };
    bulletOffset.rotate(bulletDirection);
    Bullet b(bulletPos + bulletOffset, bulletDirection, bulletVelocity);
    bullets.push_back(b);
}

void World::addBot(Bot* newBot)
{
    QMutexLocker locker(&mutex);

    unsigned int color = distinctColors[nextBotColor%distinctColors.size()];

    nextBotColor++;

    newBot->color = color;

    auto pos = randomPosition(fieldLeft, fieldRight, fieldBottom, fieldTop, newBot->radius()*1.1);

    newBot->setPos(pos);
    newBot->setDirection(randomValue(0, 2*MY_PI));

    while (collidesWithAnyBot(*newBot, 20))
    {
        auto pos = randomPosition(fieldLeft, fieldRight, fieldBottom, fieldTop, newBot->radius()*1.1);
        newBot->setPos(pos);
    }

    bots.push_back(newBot);
}


void World::addRandomBots(int count)
{
    for (int i = 1; i <= count; i ++)
    {
        //addBot(new Bot(this, new DumbAI));
    }
}


