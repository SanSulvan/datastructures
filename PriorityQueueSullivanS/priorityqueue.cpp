#include "priorityqueue.h"

PriorityQueue::PriorityQueue()
{

}

PriorityQueue::~PriorityQueue()
{

}

QueueItem& PriorityQueue::getItem(unsigned int pos)
{
    if (pos < 0 || pos > queue.size()-1)
    {
        return queue[0];
    }
    else
    {
        return queue[pos];
    }
}

int PriorityQueue::getLeftChild(unsigned int position)
{
    if (position*2+1 >= queue.size())
    {
        return -1;
    }
    else
    {
        return (position*2) + 1;
    }
}

int PriorityQueue::getRightChild(unsigned int position)
{
    if (position*2+2 >= queue.size())
    {
        return -1;
    }
    else
    {
        return (position*2) + 2;
    }
}

bool QueueItem::isHigherPriority(QueueItem item)
{
    if (priority > item.priority)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void PriorityQueue::swap(int& pos1, int& pos2)
{
    QueueItem placeHolder = getItem(pos2);
    int placeHolder2 = pos1;
    queue[pos2] = getItem(pos1);
    queue[pos1] = placeHolder;
    pos1 = pos2;
    pos2 = placeHolder2;
}

int PriorityQueue::findParent(int position)
{
    if (position == 2 || position == 1)
    {
        return 0;
    }
    else if (position == 0)
    {
        return -1;
    }
    else if (position%2 == 0)
    {
        return (position-2)/2;
    }
    else
    {
        return (position-1)/2;
    }
}

bool PriorityQueue::isHigherPriorityThan(int pos1, int pos2)
{
    return getItem(pos1).isHigherPriority(getItem(pos2));
}

void PriorityQueue::insert(QueueItem item)
{
    queue.push_back(item);

    if (queue.size() > 1)
    {
        int currentItemPosition = queue.size()-1;
        int currentParentPosition = findParent(currentItemPosition);
        QueueItem placeHolder;

        while((isHigherPriorityThan(currentItemPosition, currentParentPosition)) && currentParentPosition >= 0)
        {
            swap(currentItemPosition, currentParentPosition);
            currentParentPosition = findParent(currentItemPosition);
        }
    }
}

QueueItem PriorityQueue::remove()
{
    QueueItem head = queue[0];
    queue[0] = queue[queue.size()-1];
    int currentItemPosition = 0;
    int leftChildPosition = getLeftChild(0);
    int rightChildPosition = getRightChild(0);
    bool notDoneSwapping = true;
    while (notDoneSwapping)
    {
        if (leftChildPosition < 0 && rightChildPosition < 0)
        {
            notDoneSwapping = false;
        }
        else if (rightChildPosition < 0)
        {
            if (isHigherPriorityThan(leftChildPosition, currentItemPosition))
            {
                swap(currentItemPosition, leftChildPosition);
                rightChildPosition = getRightChild(currentItemPosition);
                leftChildPosition = getLeftChild(currentItemPosition);
            }
            else
            {
                notDoneSwapping = false;
            }
        }
        else if (leftChildPosition < 0)
        {
            if (isHigherPriorityThan(rightChildPosition, currentItemPosition))
            {
                swap(currentItemPosition, rightChildPosition);
                rightChildPosition = getRightChild(currentItemPosition);
                leftChildPosition = getLeftChild(currentItemPosition);
            }
            else
            {
                notDoneSwapping = false;
            }
        }
        else if (isHigherPriorityThan(leftChildPosition, rightChildPosition))
        {
            if (isHigherPriorityThan(leftChildPosition, currentItemPosition))
            {
                swap(currentItemPosition, leftChildPosition);
                rightChildPosition = getRightChild(currentItemPosition);
                leftChildPosition = getLeftChild(currentItemPosition);
            }
            else
            {
                notDoneSwapping = false;
            }
        }
        else
        {
            if (isHigherPriorityThan(rightChildPosition, currentItemPosition))
            {
                swap(currentItemPosition, rightChildPosition);
                rightChildPosition = getRightChild(currentItemPosition);
                leftChildPosition = getLeftChild(currentItemPosition);
            }
            else
            {
                notDoneSwapping = false;
            }
        }
    }
    int size = queue.size()-1;
    queue.erase(queue.begin() + size);
    return head;
}

int PriorityQueue::size()
{
    return queue.size();
}
