#ifndef VEC2D_H
#define VEC2D_H

class Vec2d
{
public:
    double x;   // fields (also member) (variables that are part of the class)
    double y;

    // constructor
    Vec2d();
    Vec2d(double xvalue, double yvalue);

    // methods
    double magnitude();

    void scale(double s);
    void rotate(double radians);
    void translate(Vec2d offset);
};

// some handy operators
Vec2d operator+(Vec2d p1, Vec2d p2);
Vec2d operator-(Vec2d p1, Vec2d p2);

#endif // VEC2D_H
