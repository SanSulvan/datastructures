#ifndef INTERNAL_H
#define INTERNAL_H

#include <QtWidgets>
#include <QtOpenGL>
#include <QGLWidget>
#include <QWidget>
#include <QApplication>
#include <QDesktopWidget>

class GLWidget;

class Window : public QWidget
{
    Q_OBJECT

public:
    Window(const std::string &windowName, int width, int height, int frameRate);
    virtual ~Window();

    void renderText(double x, double y, std::string text);

protected:
    void keyPressEvent(QKeyEvent *event);

private:
    QFont    *glFont;
    GLWidget *glWidget;
};

class GLWidget : public QGLWidget
{
    Q_OBJECT

    int _width;
    int _height;

    int _actualWidth;
    int _actualHeight;

    int _frameRate;
    int _framePeriodMs;

    double _lastMouseX;
    double _lastMouseY;
public:
    GLWidget(int width, int height, int frameRate);
    ~GLWidget();

    QSize minimumSizeHint() const;
    QSize sizeHint() const;

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void timerEvent(QTimerEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent* event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
private:
};


#endif // INTERNAL_H
