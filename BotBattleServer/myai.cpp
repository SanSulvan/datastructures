#include "vec2d.h"
#include "myai.h"

MyAI::MyAI()
{

}

void MyAI::handleEvents(std::vector<BotEvent> events, double currentTime)
{
    for (BotEvent& event : events)
    {
        switch (event.eventType)
        {
        case BotEventType::TurnComplete:
            Move(50,4);
            break;
        case BotEventType::MoveComplete:
            Fire();
            break;
        case BotEventType::ShotFired:
            Scan(1);
            break;
        case BotEventType::MoveBlocked:
            // not needed if BotCollision and WallCollision are handled
            break;
        case BotEventType::ScanComplete:
            Move(50,4);
            break;
        case BotEventType::BotCollision:
            Fire();
            break;
        case BotEventType::WallCollision:
            Turn(MY_PI/2);
            break;
        case BotEventType::BulletCollision:
            break;
        }
    }
}

