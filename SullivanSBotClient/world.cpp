#include <limits>
#include "graphics.h"
#include "world.h"
#include <algorithm>
#include "mymath.h"
#include <iostream>
#include "botai.h"
#include <sstream>

extern Vec2d mousePos;  // set automatically to the current mouse location
extern double cameraAngle;

using namespace std;
#include <QMetaType>


BotEvent::BotEvent(BotEventType eventType, double eventTime, double collisionAngle, double travelDistance, const CameraView& cv)
{
    this->eventType = eventType;
    this->eventTime = eventTime;
    this->collisionAngle = collisionAngle;
    this->travelDistance = travelDistance;
    view = cv;
}

void read(CameraView& view, std::istream& strm)
{
    int sz;

    strm >> sz;

    view.color.resize(sz, 0);

    for (int i=0;i<sz;i++)
    {
        char c;
        int id = 0;
        strm >> c;
        if (c == '_')
        {
            id = 0;
        }
        else
        {
            id = c - '1' + 1;
        }
        view.color[i] = id;
    }
}

void write(CameraView& view, std::ostream& strm)
{
    strm << (int)view.color.size() << " ";
    for (size_t i=0;i<view.color.size();i++)
    {
        if (view.color[i] == 0)
        {
            strm << "_";
        }
        else
        {
            strm << view.color[i];
        }
    }
}

void BotEvent::write(std::ostream& strm)
{
    switch (eventType)
    {
    case BotEventType::TurnComplete:
        strm << "Turned ";
        strm << eventTime;
        break;
    case BotEventType::MoveComplete:
        strm << "Moved ";
        strm << eventTime;
        break;
    case BotEventType::ShotFired:
        strm << "Fired ";
        strm << eventTime;
        break;
    case BotEventType::MoveBlocked:
        strm << "Blocked ";
        strm << eventTime;
        break;
    case BotEventType::ScanComplete:
        strm << "Scanned ";
        strm << eventTime;
        ::write(view, strm);
        break;
    case BotEventType::BotCollision:
        strm << "Bonk ";
        strm << eventTime << " " << collisionAngle;
        break;
    case BotEventType::WallCollision:
        strm << "Wall ";
        strm << eventTime << " " << collisionAngle;
        break;
    case BotEventType::BulletCollision:
        strm << "Shot ";
        strm << eventTime << " " << collisionAngle;
        break;
    }

    strm << "\r\n";
}

BotEvent BotEvent::read(std::istream& strm)
{
    BotEventType eventType;

    std::string et;

    strm >> et;

    if (et == "Turned")       { eventType = BotEventType::TurnComplete; }
    else if (et == "Moved")   { eventType = BotEventType::MoveComplete; }
    else if (et == "Fired")   { eventType = BotEventType::ShotFired; }
    else if (et == "Blocked") { eventType = BotEventType::MoveBlocked; }
    else if (et == "Scanned") { eventType = BotEventType::ScanComplete; }
    else if (et == "Bonk")    { eventType = BotEventType::BotCollision; }
    else if (et == "Wall")    { eventType = BotEventType::WallCollision; }
    else if (et == "Shot")    { eventType = BotEventType::BulletCollision; }
    else
    {
        // error
        return BotEvent(BotEventType::MoveBlocked, 0, 0, 0);
    }

    double eventTime;
    double collisionAngle = 0;
    double travelDistance = 0;
    CameraView view;

    strm >> eventTime;

    switch (eventType)
    {
    case BotEventType::TurnComplete:
    case BotEventType::ShotFired:
        // nothing else to read
        break;
    case BotEventType::MoveComplete:
    case BotEventType::MoveBlocked:
        strm >> travelDistance;
        break;
    case BotEventType::ScanComplete:
        ::read(view, strm);
        break;
    case BotEventType::BotCollision:
    case BotEventType::WallCollision:
    case BotEventType::BulletCollision:
        strm >> collisionAngle;
        strm >> travelDistance;
        break;
    }

    return BotEvent(eventType, eventTime, collisionAngle, travelDistance, view);
}


std::vector<unsigned int> distinctColors =
{
    // https://eleanormaclure.files.wordpress.com/2011/03/colour-coding.pdf

        0xFFB300, // Vivid Yellow
        0x803E75, // Strong Purple
        0xFF6800, // Vivid Orange
        0xA6BDD7, // Very Light Blue
        0xC10020, // Vivid Red
        0xCEA262, // Grayish Yellow
        0x817066, // Medium Gray

        // The following don't work well for people with defective color vision
        0x007D34, // Vivid Green
        0xF6768E, // Strong Purplish Pink
        0x00538A, // Strong Blue
        0xFF7A5C, // Strong Yellowish Pink
        0x53377A, // Strong Violet
        0xFF8E00, // Vivid Orange Yellow
        0xB32851, // Strong Purplish Red
        0xF4C800, // Vivid Greenish Yellow
        0x7F180D, // Strong Reddish Brown
        0x93AA00, // Vivid Yellowish Green
        0x593315, // Deep Yellowish Brown
        0xF13A13, // Vivid Reddish Orange
        0x232C16, // Dark Olive Green

};

int generateRandom(int maxValue);

Vec2d randomPosition(double minx, double maxx, double miny, double maxy)
{
    double x = minx + generateRandom(maxx - minx);
    double y = miny + generateRandom(maxy - miny);

    return Vec2d { x, y };
}

Vec2d randomPosition(double minx, double maxx, double miny, double maxy, double marginSize)
{
    return randomPosition(minx + marginSize, maxx - marginSize, miny + marginSize, maxy - marginSize);
}

double randomValue(double minValue, double maxValue)
{
    double p = (double)generateRandom(RAND_MAX-1) / RAND_MAX;
    double v = minValue + p * (maxValue - minValue);
    if (v <= minValue)
    {
        v = minValue;
    }
    if (v >= maxValue)
    {
        v = maxValue;
    }
    return v;
}

double subtractAngles(double a1, double a2)
{
    double diff = a1 - a2;

    if (diff < -MY_PI)
    {
        diff += 2*MY_PI;
    }

    if (diff > MY_PI)
    {
        diff -= 2*MY_PI;
    }

    return diff;
}

void World::socketStateChanged(QAbstractSocket::SocketState ss)
{
    qDebug() << "Socket State Changed: " << (int)ss;
}

void World::socketError(QAbstractSocket::SocketError se)
{
    qDebug() << "Socket Error Signal: " << (int)se;
}


World::World(double fieldTop, double fieldBottom, double fieldLeft, double fieldRight, QObject *parent) : QObject(parent)
{
    qRegisterMetaType< QAbstractSocket::SocketState >();
    qRegisterMetaType< QAbstractSocket::SocketError >();


    this->fieldTop    = fieldTop;
    this->fieldBottom = fieldBottom;
    this->fieldLeft   = fieldLeft;
    this->fieldRight  = fieldRight;

    socket = new QTcpSocket(this);

    socket->connectToHost("192.168.6.113", 1234);

    if(socket->waitForConnected(5000))
    {
        qDebug() << "Connected!";

        gotDisconnected = false;

        connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()), Qt::QueuedConnection);
        connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()), Qt::QueuedConnection);
        connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(socketStateChanged(QAbstractSocket::SocketState)), Qt::QueuedConnection);
        connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(socketError(QAbstractSocket::SocketError)), Qt::QueuedConnection);

        std::string nameCmd = "Name " + myAi.botName + "\n";

        socket->write(nameCmd.c_str());

        socket->waitForBytesWritten(1000);
        //socket->waitForReadyRead(3000);

    }
    else
    {
        qDebug() << "Not connected! " << socket->errorString();
        gotDisconnected = true;
    }
}

World::~World()
{
    socket->close();
}

QByteArray clean(QByteArray data)
{
     for (int i=0;i<data.size();i++)
     {
         switch (data[i])
         {
         case '\n':
         case '\r':
             data[i] = ' ';
             break;
         }
     }

     return data;
}

void World::sendData(std::string data)
{
    if (socket)
    {
        std::cout << "Sending Cmd: " << data;
        socket->write(data.c_str());
        socket->flush();
        //qDebug() << "Sending: " << clean(data);
    }
    else
    {
        //qDebug() << "Not Sending (disconnected): " << clean(data);
    }

}

void World::readyRead()
{
    // get the information

    QByteArray newdata = socket->readAll();

   // qDebug() << " Some data received: " << clean(newdata);

    {
        QMutexLocker locker(&mutex);

        receivedData.append(newdata.constData());

        auto idx = receivedData.find_first_of('\n');
        while (idx != string::npos)
        {
            commandQueue.push_back(receivedData.substr(0, idx));
           // qDebug() << " Command Queued: " << clean(QByteArray::fromStdString(commandQueue.back()));
            receivedData = receivedData.substr(idx+1);
            idx = receivedData.find_first_of('\n');
        }
    }
}

void World::disconnected()
{
    qDebug() << "Server Disconnected";
    gotDisconnected = true;
}

void World::update()
{
   // qDebug() << "Update: " << elapsedTime;

    //socket->waitForReadyRead(0);

    while (true)
    {
        string cmd;

        {
            QMutexLocker locker(&mutex);
            if (commandQueue.size() > 0)
            {
                cmd = commandQueue.front();
                commandQueue.erase(commandQueue.begin());
            }
        }

        if (cmd.empty())
        {
           // qDebug() << "No data\n";
            return;
        }

        //qDebug() << "Handling Response: " << cmd.c_str();

        handleResponse(cmd);
    }
}

void World::handleResponse(string resp)
{
    std::stringstream ss(resp);

    if (resp[0] == '.')
    {
        std::string junk;
        double currentTime;

        ss >> junk;
        ss >> currentTime;

        // process events
        myAi.handleEvents(events, currentTime);

        events.clear();

        BotCmdType cmdType;
        double arg1;
        double arg2;

        myAi.getCmd(cmdType, arg1, arg2);

        std::stringstream ss;

        switch (cmdType)
        {
        case BotCmdType::Fire:
            sendData("Fire\n");
            break;
        case BotCmdType::Look:
            ss << "Scan " << arg1 << "\n";
            sendData(ss.str());
            break;
        case BotCmdType::Turn:
            ss << "Turn " << arg1 << "\n";
            sendData(ss.str());
            break;
        case BotCmdType::Move:
            ss << "Move " << arg1 << " " << arg2 << "\n";
            sendData(ss.str());
            break;
        case BotCmdType::NoCommand:
            break;
        case BotCmdType::Disconnected:
            break;
        }
    }
    else
    {
        std::stringstream ss(resp);

        BotEvent ev = BotEvent::read(ss);

        events.push_back(ev);

        ss.str("");

        ev.write(ss);

        //std::cout << "Got Event: " << ss.str();

    }
}



void World::draw(double /* elapsedTime */)
{
    drawRectangle({fieldLeft, fieldTop},{fieldRight, fieldBottom}, 0xFFFFFF);

    if (lastView.size() > 0)
    {
        double PixelWidth = (fieldRight - fieldLeft)/lastView.size();

        for (unsigned int i=0;i<lastView.size();i++)
        {
            fillRectangle({fieldLeft + i*PixelWidth, fieldBottom - 20}, {fieldLeft + i*PixelWidth+PixelWidth, fieldBottom}, lastView[i]);
        }
    }
}



