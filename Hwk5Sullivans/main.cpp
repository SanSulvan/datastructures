#include <iostream>
#include <string>

using namespace std;

class Node
{
public:
    double value;
    Node*  next;
    Node();
   ~Node();
};


Node::Node()
{
    cout << "Node created\n";
}

Node::~Node()
{
    cout << "Node " << value << " destroyed\n";
}


class List
{
public:
    Node* head;
    List();
   ~List();
    void push(double value);
    void print();
    double lastValue();
    void append(double value);
    void deleteValue(double value);
    void insert(double value); // assuming list is sorted!!!
};


List::List()
{
    head = nullptr;
    cout << "I'm Alive!!!\n";
}

List::~List()
{
    Node* currentNode = head;

    while (currentNode != nullptr)
    {
        Node* next = currentNode->next;
        delete currentNode;
        currentNode = next;
    }

    cout << "I'm dying!!!\n";
}


void List::deleteValue(double value)
{
    Node* currentNode = head;
    bool deleted = false;
    Node* deletedNode = nullptr;

    std::cout << "beginning of deleteValue";
    if (head != nullptr && head->value == value)
    {
        std::cout << "test 2" << endl;
        deletedNode = head;
        cout << endl << head->next << endl;
        head = head->next;
        delete currentNode;
        deleted = true;
    }

    while (currentNode->next != nullptr  && deleted == false)
    {
        std::cout << "test 3" << endl;
        if (currentNode->next->value == value)
        {
            deletedNode = currentNode->next;
            Node* nodeAfter = currentNode->next->next;
            delete currentNode->next;
            currentNode->next = nodeAfter;
            deleted = true;
        }

        if (currentNode->next != nullptr)
        {
            currentNode = currentNode->next;
        }
    }

    if (deleted)
    {
        std::cout << "The first instance of the value " << value << " was deleted at location " << deletedNode << "\n\n";
    }
    else
    {
        std::cout << "There were no instances of the value " << value << " in this list! Sorry\n\n";
    }

    std::cout << head;

    return;
}

void List::append(double value)
{
    Node* currentNode = head;

    if (currentNode == nullptr)
    {
        push(value);
        return;
    }

    Node* node = new Node;

    node->value = value;
    node->next = nullptr;

    while (currentNode->next != nullptr)
    {
        currentNode = currentNode->next;
    }


    if (currentNode->next != nullptr)
    {
        currentNode = currentNode->next;
    }

}

double List::lastValue()
{
    Node* currentNode = head;

    if (currentNode == nullptr)
    {
        return 0.0;
    }

    while (currentNode->next != nullptr)
    {
        currentNode = currentNode->next;
    }


    return currentNode->value;
}

void List::print()
{
    Node* currentNode = head;

    while (currentNode != nullptr)
    {
        cout << currentNode->value << endl;
        currentNode = currentNode->next;
    }
}



void List::push(double value)
{
    Node* node = new Node();

    node->value = value;
    node->next  = head;

    head = node;
}



int main()
{
    List myList;

    myList.append(100);
    myList.append(101);
    myList.append(102);
    myList.append(103);

    myList.deleteValue(100);
    std::cout << "wtf??" << endl;
    myList.deleteValue(101);
    myList.deleteValue(103);

    cout << "\n\n" << "The list contains: ";
    myList.print();

    return 0;
}
