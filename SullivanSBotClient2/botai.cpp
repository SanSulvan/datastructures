#include "botai.h"

BotAI::BotAI()
{
    botName = "MELEE_CRUISER";
    disconnected = false;
    setCmd(BotCmdType::Move, 0, 0.1);
    readyToFire = false;
    wallBlocked = false;
    botBlocked = false;
    turnAngle = 0;
    scanAngle = 2*M_PI/3;
    enemySpotted = -1;
    initializing = true;
    beenHit = false;
}

void BotAI::setCmd(BotCmdType cmd, double arg1, double arg2)
{
    QMutexLocker locker(&mutex);

    nextCommand     = cmd;
    nextCommandArg1 = arg1;
    nextCommandArg2 = arg2;
}

void BotAI::getCmd(BotCmdType& cmd, double& arg1, double& arg2)
{
    QMutexLocker locker(&mutex);

    if (disconnected)
    {
        cmd  = BotCmdType::Disconnected;
        arg1 = 0;
        arg2 = 0;
    }
    else
    {
        cmd = nextCommand;
        arg1 = nextCommandArg1;
        arg2 = nextCommandArg2;
    }
}

void BotAI::handleEvents(std::vector<BotEvent> events, double currentTime)
{
    // each time "handleEvents" is called, you will get a list
    // of one or more events (often just a single event)

    // It is important to call exactly of your actions...
    //    Fire
    //    Move
    //    Turn
    //    Scan
    //
    // ... Whenever you get any of the following events:
    //
    //    TurnComplete
    //    MoveComplete
    //    ShotFired
    //    MoveBlocked
    //    ScanComplete
    //
    // ... 'cause if you don't, your bot might just stop.
    //
    //
    // ... these events just give you extra information...
    //    you don't necessarily need to react to them.
    //
    //    BotCollision:
    //    WallCollision:
    //    BulletCollision:
    //

    for (BotEvent& event : events)
    {
        switch (event.eventType)
        {
        case BotEventType::TurnComplete:
            // you finished turning
            if (beenHit)
            {
                beenHit = false;
                Move(50, 1000);
            }
            else if (botBlocked || readyToFire)
            {
                readyToFire = false;
                botBlocked = false;
                Fire();
            }
            else if (wallBlocked)
            {
                Move(50, 1000);
            }
            else
            {
                Move(50, 1000);
            }
            break;
        case BotEventType::MoveComplete:
            // you finished a move normally, without hitting anything
            Move(50, 1000);
            break;
        case BotEventType::ShotFired:
            // your gun just fired
            if (botBlocked)
            {
                Fire();
            }
            Move(50, 1000);
            break;
        case BotEventType::MoveBlocked:
            if (initializing)
            {
                initializing = false;
                Move(50, 1000);
            }
            break;
        case BotEventType::ScanComplete:
            lastCamera = event.view.color;
            enemySpotted = -1;
            for (int i = 0; i < lastCamera.size(); i++)
            {
                if (lastCamera[i] != 0 && enemySpotted == -1)
                {
                    enemySpotted = i;
                }
            }

            if (enemySpotted >= 0)
            {
                turnAngle = (enemySpotted*scanAngle/lastCamera.size())-(scanAngle/2)+(M_PI*1/180);
                readyToFire = true;
            }
            else
            {
                readyToFire = false;
                turnAngle = M_PI/6;
            }

            Turn(-turnAngle);
            break;
        case BotEventType::BotCollision:
            turnAngle = event.collisionAngle;
            botBlocked = true;
            Turn(turnAngle);
            break;
        case BotEventType::WallCollision:
            turnAngle = M_PI/2 - event.collisionAngle;
            wallBlocked = true;
            botBlocked = false;
            readyToFire = false;
            Turn(-turnAngle);
            break;
        case BotEventType::BulletCollision:
            if (botBlocked)
            {
                beenHit = true;
                Turn(M_PI/2);
            }
            else if (fabs(event.collisionAngle) > M_PI/4 || event.collisionAngle == M_PI*2)
            {
                std::cout << std::endl << event.collisionAngle << std::endl;
                Move(50, 1000);
            }
            else
            {
                beenHit = true;
                Turn(-M_PI/5);
            }
            break;
        }
    }
}



void BotAI::Turn(double angle)
{
    setCmd(BotCmdType::Turn, angle, 0);
}

void BotAI::Move(double speed, double time)
{
   if (speed < -50)
   {
       speed = -50;
   }
   if (speed > 50)
   {
       speed = 50;
   }
   setCmd(BotCmdType::Move, speed, time);
}

void BotAI::Fire()
{
   setCmd(BotCmdType::Fire, 2, 0);
}

void BotAI::Scan(double fieldOfView)
{
    if (fieldOfView < MY_PI/180)
    {
        fieldOfView = MY_PI/180;
    }
    if (fieldOfView > MY_PI)
    {
        fieldOfView = MY_PI;
    }
    setCmd(BotCmdType::Look, fieldOfView, 0);
}

void BotAI::ResetCmd()
{
    setCmd(BotCmdType::NoCommand, 0, 0);
}

