#ifndef ET_AI_H
#define ET_AI_H

#include "botai.h"

class ET_AI : public BotAI
{
public:
    ET_AI();

    void whenShotFired(double elapsedTime);
    void whenScanCompleted(double elapsedTime, std::vector<int> view);
    void whenTurnCompleted(double elapsedTime);
    void whenMoveBlocked(double elapsedTime);
    void whileIdle(double elapsedTime);
    void whileMoving(double elapsedTime);
};

#endif // ET_AI_H
