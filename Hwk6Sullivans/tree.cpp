#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    double value;
    Node* left;
    Node* right;
    void inOrder(vector<double>& vec);
    Node();
    ~Node();
};

Node::Node()
{
    right = nullptr;
    left = nullptr;
    cout << "\nNode created!\n";
}

Node::~Node()
{
    cout << "\nNode with value " << value << " destroyed!\n";
}

class Tree
{
public:
    Node* root;
    void insert(double value);
    void inOrder(vector<double>& vec);
    Node* findValueAndParent(double value, Node*& parentNode);
    void deleteValue(double value);
    Tree();
    ~Tree();
};

Tree::Tree()
{
    root = nullptr;
    cout << "\nTree created!\n";
}

Tree::~Tree()
{
    //destroy tree!!!
    vector<double> vec;
    inOrder(vec);
    for (int i = 0; i < vec.size(); i++)
    {
        deleteValue(i);
    }
}

void Tree::insert(double value)
{
    Node* currentNode = root;

    Node* newNode = new Node();
    newNode->value = value;

    if (currentNode == nullptr)
    {
        root = newNode;
        return;
    }

    while (currentNode != nullptr)
    {
        if (value > currentNode->value)
        {
            if (currentNode->right == nullptr)
            {
                currentNode->right = newNode;
                currentNode = nullptr;
            }
            else
            {
                currentNode = currentNode->right;
            }
        }
        else if (value < currentNode->value)
        {
            if (currentNode->left == nullptr)
            {
                currentNode->left = newNode;
                currentNode = nullptr;
            }
            else
            {
                currentNode = currentNode->left;
            }
        }
        else
        {
            cout << "\nSorry, there was an error inserting the value " << value << ". Most likely it was already in the tree!\n";
            currentNode = nullptr;
        }
    }
}

void Node::inOrder(vector<double>& vec)
{
    if (left != nullptr)
    {
        left->inOrder(vec);
    }
    vec.push_back(value);
    if (right != nullptr)
    {
        right->inOrder(vec);
    }
}

void Tree::inOrder(vector<double>& vec)
{
    root->inOrder(vec);
}

Node* Tree::findValueAndParent(double value, Node*& parentNode)
{
    Node* currentNode = root;
    parentNode = nullptr;

    if (currentNode == nullptr)
    {
        cout << "\nThis tree is empty!\n";
        return currentNode;
    }

    if (currentNode->value == value)
    {
        return currentNode;
    }

    while (currentNode->value != value)
    {
        if (currentNode->value < value)
        {
            if (currentNode->right != nullptr && currentNode->right->value == value)
            {
                parentNode = currentNode;
                currentNode = currentNode->right;
            }
            else
            {
                currentNode = currentNode->right;
            }
        }
        else if (currentNode->value > value)
        {
            if (currentNode->left != nullptr && currentNode->left->value == value)
            {
                parentNode = currentNode;
                currentNode = currentNode->left;
            }
            else
            {
                currentNode = currentNode->left;
            }
        }
        if (currentNode == nullptr)
        {
            cout << "\nSorry, the value you asked for is not in the tree!\n";
            return currentNode;
        }
    }

    return currentNode;
}



void Tree::deleteValue(double value)
{
    Node* parentNode = nullptr;
    Node* deleteNode = findValueAndParent(value, parentNode);
    Node* currentNode = nullptr;

    if (deleteNode == nullptr)
    {
        return;
    }

    if (parentNode == nullptr)
    {
        if (deleteNode->right == nullptr)
        {
            root = deleteNode->left;
            delete deleteNode;
            return;
        }
        else if (deleteNode->left == nullptr)
        {
            root = deleteNode->right;
            delete deleteNode;
        }
        else
        {
            root = deleteNode->right;
            currentNode = deleteNode->right;
            while (currentNode->left != nullptr)
            {
                currentNode = currentNode->left;
            }
            currentNode->left = deleteNode->left;
            delete deleteNode;
        }
    }

    if (deleteNode->right == nullptr && deleteNode->left == nullptr)
    {
        if (parentNode->value < deleteNode->value)
        {
            delete parentNode->right;
            parentNode->right = nullptr;
        }
        else
        {
            delete parentNode->left;
            parentNode->left = nullptr;
        }
    }
    else if (deleteNode->right != nullptr)
    {
        if (parentNode->value < deleteNode->value)
        {
            parentNode->right = deleteNode->right;
            currentNode = deleteNode->right;
            while (currentNode->left != nullptr)
            {
                currentNode = currentNode->left;
            }
            currentNode->left = deleteNode->left;
            delete deleteNode;
        }
        else if (parentNode->value > deleteNode->value)
        {
            parentNode->left = deleteNode->right;
            currentNode = deleteNode->right;
            while (currentNode->left != nullptr)
            {
                currentNode = currentNode->left;
            }
            currentNode->left = deleteNode->left;
            delete deleteNode;
        }
    }
    else if (deleteNode->right == nullptr)
    {
        if (parentNode->value < deleteNode->value)
        {
            parentNode->right = deleteNode->left;
            delete deleteNode;
        }
        else if (parentNode->value > deleteNode->value)
        {
            parentNode->left = deleteNode->left;
            delete deleteNode;
        }
    }
}
