#include "catch.hpp"
#include <vector>
#include <cstdlib>
#include <iostream>
#include <ctime>
#include "sortfunction.cpp"

TEST_CASE( "Testing Some Sorting", "[vector]" )
{

    std::vector<double> originalV = {
        3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5, 8, 9, 7, 9, 3, 2, 3, 8, 4, 6, 2, 6, 4,
        3, 3, 8, 3, 2, 7, 9, 5, 0, 2, 8, 8, 4, 1, 9, 7, 1, 6, 9, 3, 9, 9, 3, 7,
        5, 1, 0, 5, 8, 2, 0, 9, 7, 4, 9, 4, 4, 5, 9, 2, 3, 0, 7, 8, 1, 6, 4, 0,
        6, 2, 8, 6, 2, 0, 0, 8, 9, 9, 8, 6, 2, 8, 0, 3, 4, 8, 2, 5, 3, 4, 2, 1
    };

    SECTION( "Sorting Order" )
    {
        std::vector<double> newV = originalV;

        sort(newV);

        for (int i = 0; i < newV.size(); i++)
        {
            std::cout << originalV[i] << " ";
        }

        std::cout << std::endl << "SEPERATOR" << std::endl;

        for (int i = 0; i < newV.size(); i++)
        {
            std::cout << newV[i] << " ";
        }

        for (int i = 0; i < newV.size()-1; i++)
        {
            REQUIRE ( newV[i] <= newV[i+1] );
        }
    }

    SECTION( "Element Preservation" )
    {
        std::vector<double> newV = originalV;

        REQUIRE( newV.size() == originalV.size() );

        double x;
        bool found;

        for (int i = 0; i < originalV.size(); i++)
        {
            x = originalV[i];
            found = false;

            for (int n = 0; n < newV.size() && !found; n++)
            {
                if (newV[n] == x)
                {
                    newV.erase(newV.begin() + n);
                    found = true;
                    break;
                }
            }

            REQUIRE( found );
        }
    }
}
