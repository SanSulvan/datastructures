#include "vec2d.h"
#include "PinkRanger.h"

PinkRanger::PinkRanger()
{

}

/*

    Fire();
Scan(fieldOfView); // maximum field of view is given by maxFieldOfView function
Move(speed);       // maximum speed is given by maxSpeed function
Turn(radians);

*/

void PinkRanger::whenShotFired(double /* elapsedTime */)
{
    Turn(M_PI/2);
    Move(-25);
}

void PinkRanger::whenScanCompleted(double /* elapsedTime */, std::vector<int> /* view */)
{
    Move(50);
    Fire();
}

void PinkRanger::whenTurnCompleted(double /* elapsedTime */)
{
    Scan(M_PI/2);
    Fire();
}

void PinkRanger::whenMoveBlocked(double /* elapsedTime */)
{
    Fire();

}

void PinkRanger::whileIdle(double /* elapsedTime */)
{
    Scan(M_PI/2);
    Fire();

}

void PinkRanger::whileMoving(double /* elapsedTime */)
{
    Fire();
    Fire();
    Fire();
    Fire();
    Fire();
    Fire();
    Fire();

}
