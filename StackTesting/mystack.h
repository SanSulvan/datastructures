#ifndef MYSTACK_H
#define MYSTACK_H


class MyStack
{
private:
    int     size; // size of the values array
    double *values;
    int     top; // index of the top value
public:
    MyStack();
    ~MyStack();
    void push(double value);
    double pop();
    double peek();
    bool isEmpty();
};

#endif // MYSTACK_H
