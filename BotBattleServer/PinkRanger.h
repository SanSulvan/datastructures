#ifndef PinkRanger_H
#define PinkRanger_H

#include "botai.h"

class PinkRanger : public BotAI
{
public:
    PinkRanger();

    void whenShotFired(double elapsedTime);
    void whenScanCompleted(double elapsedTime, std::vector<int> view);
    void whenTurnCompleted(double elapsedTime);
    void whenMoveBlocked(double elapsedTime);
    void whileIdle(double elapsedTime);
    void whileMoving(double elapsedTime);
};

#endif // PinkRanger_H
