#ifndef BOT_H
#define BOT_H

#include <string>
#include "vec2d.h"
#include "botcmd.h"
#include "botai.h"
#include "cameraview.h"

class World;
class Bot;
class CameraView;

class Bullet
{
public:
    Vec2d        position;
    Vec2d        velocity;
    bool         collided;
    double       angle;
public:
    Bullet(Vec2d pos, double angle, Vec2d velocity);
    void draw();
    bool collidesWith(Bot& bot);
    void update(double elapsedTime);
};





class Bot
{
public:
    std::string  name;
    int          botId;
    Vec2d        velocity;
    Vec2d        position;
    double       direction;
    unsigned int color;
    double       diameter;
    int          health;
    bool         disconnected;
public:
    Vec2d        moveStartPos;
    Vec2d        moveEndPos;
    Vec2d        lastPos;
    bool         collided;

    double       collideAngle;
    double       collideEffect;
    double       shotEffect;

    BotCmd      *command;

    BotAI*       brain;

    std::vector<BotEvent> events;

    World *world;

public:
    Bot(World *world, BotAI* brain);
   ~Bot();
    unsigned int getColor() { return color; }
    void draw(double elapsedTime);
    Vec2d getPos() { return position; }

    void setPos(Vec2d pos);

    bool collidesWith(Bot& otherBot, double margin = 0.0);

    double radius() { return diameter / 2; }

    void setDirection(double dir) { direction= dir; }

    void giveCommand(BotCmdType cmd, double arg1, double arg2);

    void useBrainCommand();

    void update(World& world, double currentTime, double elapsedTime);

    void execMove(double eventTime, double angle, double distance, bool isComplete);
    void execTurn(double eventTime, double angle, bool isComplete);
    void execFire(double eventTime);
    void execScan(double eventTime, double fov);

    void gotShot(double eventTime, double bulletAngle);
    void hitWall(double eventTime, double wallNormalAngle);
    void hitBot(double eventTime, double otherBotDirection);
    void gotStopped(double eventTime);

    bool isDead()         { return health <= 0;  }
    bool isDisconnected() { return disconnected; }

    void processEvents(double currentTime);
};

#endif // BOT_H
