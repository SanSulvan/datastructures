#include "vec2d.h"
#include "graphics.h"
#include <cmath>
#include <vector>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include "maze.h"

int screenWidth = 1000;
int screenHeight = 500;

void initializeRandomNumberGenerator() // put this in your main
{
    srand(time(0));
}

int generateRandom(int maxValue)  // generate a number between 0 and maxValue
{
    return rand()%(maxValue+1);
}

using namespace std;

Vec2d mousePos;  // set automatically to the current mouse location

Maze maze(40,20);
std::vector<RowCol> thePath;

void drawMaze(std::vector<RowCol>& path, double cellSize)
{
    for (int i = path.size(); i > 0; i--)
    {
        Vec2d p1;
        Vec2d p2;
        p1.x = -(screenWidth/2) + cellSize*(path[i].col+.5);
        p1.y = (screenHeight/2) - cellSize*(path[i].row+.5);
        p2.x = -(screenWidth/2) + cellSize*(path[i-1].col+.5);
        p2.y = (screenHeight/2) - cellSize*(path[i-1].row+.5);
        drawLine(p1, p2, 0x00FF00);
    }
}

void updateDisplay(int /* elapsedMs */)
{

    int startx = -screenWidth/2;
    int starty =  screenHeight/2;

    double cellSize = std::min(screenWidth / maze.width(), screenHeight / maze.height());

    for (int r = 0; r < maze.height(); r++)
    {
        for (int c = 0; c < maze.width(); c++)
        {
            // draw a cell's walls
            bool hasTop = maze.hasWall({r,c},Direction::Up);

            if (hasTop)
            {
                drawLine({startx + cellSize * c, starty - cellSize * r},
                         {startx + cellSize * (c+1), starty - cellSize * r}, 0xFFFFFF);
            }


            bool hasLeft = maze.hasWall({r,c},Direction::Left);

            if (hasLeft)
            {
                drawLine({startx + cellSize * c, starty - cellSize * r},
                         {startx + cellSize * c, starty - cellSize * (r + 1)}, 0xFFFFFF);
            }

        }
    }
    if (thePath.size() > 1)
    {
        drawMaze(thePath, cellSize);
    }
}


void keyboardFunc(unsigned char key, int /*x*/, int /*y*/)
{
    switch (key)
    {
    case 'g':
        break;
    case 'a':
        break;
    case 'd':
        break;
    case 's':
        break;
    }
}

void keyboardUpFunc(unsigned char /* key */, int /*x*/, int /*y*/)
{
}

void mouseFunc(int /*button*/, int /*state*/, int /*x*/, int /*y*/)
{
}

void motionFunc(int /*x*/, int /*y*/)
{
}

void passiveMotionFunc(int /*x*/, int /*y*/)
{
}

int main()
{
    initializeRandomNumberGenerator();

    maze.buildMaze();

    if (!maze.testWalls())
    {
        std::cout << "Maze data is corrupt!\n";
    }
    else
    {
        std::cout << "Maze walls seem consistent\n";
    }

    thePath.clear();
    maze.solveMaze(thePath);
    std::cout << thePath.size();

    beginGraphicsLoop("Hello World", screenWidth, screenHeight, 24);
    return 0;
}
