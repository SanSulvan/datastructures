#-------------------------------------------------
#
# Project created by QtCreator 2015-03-02T13:54:42
#
#-------------------------------------------------


CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT += core
QT += opengl
QT += network

TARGET = GameField
TEMPLATE = app


CONFIG += console

SOURCES += \
    font.cpp \
    graphics.cpp \
    ImageDraw.cpp \
    main.cpp \
    vec2d.cpp \
    world.cpp \
    botai.cpp \
    cameraview.cpp

HEADERS  += \
    font.h \
    graphics.h \
    ImageDraw.h \
    internal.h \
    vec2d.h \
    world.h \
    botai.h \
    botcmd.h \
    mymath.h \
    cameraview.h

