#include "vec2d.h"
#include "graphics.h"
#include <cmath>
#include <vector>
#include <iostream>
#include <sstream>
#include "ImageDraw.h"
#include <cstdlib>
#include <ctime>
#include <algorithm>

void initializeRandomNumberGenerator() // put this in your main
{
    srand(time(0));
}

int generateRandom(int maxValue)  // generate a number between 0 and maxValue
{
    return rand()%(maxValue+1);
}

using namespace std;

Vec2d mousePos;  // set automatically to the current mouse location

class CPoint
{
public:
    Vec2d location;
    int   color;
};

std::vector<CPoint> points;
CPoint closest1;
CPoint closest2;


void updateDisplay(int /* elapsedMs */)
{
    for (auto& p : points)
    {
        drawPoint(p.location, p.color);
    }

    // draw a line between closest1, closest2
}

bool compareX(CPoint& p1, CPoint& p2)
{
    return p1.location.x < p2.location.x;
}

void sortByX(std::vector<CPoint>& points)
{
    std::sort(points.begin(), points.end(), compareX);
}

void findClosest(std::vector<CPoint> points, CPoint& p1, CPoint& p2)
{
    if (points.size() == 3)
    {
        // brute force
    }
    else if (points.size() == 2)
    {
        // obvious
    }

    // split it in half

    // find closest on each half

    // find min delta

    // sort by y
    // filter based on distance from center point

    // loop over those points to find distances with adjacent points

    // return the best
}

void generatePoints(int count)
{
    points.clear();

    for (int i =0; i< count; i++)
    {
        CPoint p;
        p.color    = 0xFFFFFF; // generateRandom(0xFFFFFF);
        p.location.x = generateRandom(1000) - 500;
        p.location.y = generateRandom(500) - 250;

        points.push_back(p);
    }


    sortByX(points);

    findClosest(points, closest1, closest2);
}

void keyboardFunc(unsigned char key, int /*x*/, int /*y*/)
{
    switch (key)
    {
    case 'g':
        generatePoints(500);
        break;
    case 'a':
        break;
    case 'd':
        break;
    case 's':
        break;
    }
}

void keyboardUpFunc(unsigned char /* key */, int /*x*/, int /*y*/)
{
}

void mouseFunc(int /*button*/, int /*state*/, int /*x*/, int /*y*/)
{
}

void motionFunc(int /*x*/, int /*y*/)
{
}

void passiveMotionFunc(int /*x*/, int /*y*/)
{
}

int main()
{
    initializeRandomNumberGenerator();


    beginGraphicsLoop("Hello World", 1000, 500, 24);
    return 0;
}
