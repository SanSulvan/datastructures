#ifndef BOTAI_H
#define BOTAI_H

#include <vector>

#define _USE_MATH_DEFINES
#include <cmath>

#include "mymath.h"

#include <QMutex>
#include <QMutexLocker>
#include <QObject>

#include "botcmd.h"


class BotAI : public QObject
{
    Q_OBJECT

private:

    bool         disconnected;
    BotCmdType   nextCommand;
    double       nextCommandArg1;
    double       nextCommandArg2;

public:
    std::string  botName;
    bool readyToFire;
    bool wallBlocked;
    bool botBlocked;
    double turnAngle;
    double scanAngle;
    std::vector<int> lastCamera;
    int enemySpotted;
    bool initializing;
    double testVariable;
    bool beenHit;

protected:

    QMutex       mutex;

public:

    BotAI();

    virtual ~BotAI() { }

    void setCmd(BotCmdType cmd, double arg1, double arg2);
    void getCmd(BotCmdType& cmd, double& arg1, double& arg2);
    void ResetCmd();

    virtual void handleEvents(std::vector<BotEvent> events, double currentTime);

    //virtual void asyncWait();
protected:

    void Turn(double angle);
    void Move(double speed, double time = 1000);
    void Fire();
    void Scan(double fieldOfView);

    double maxSpeed()       { return 50; }
    double maxFieldOfView() { return MY_PI; }

};


#endif // BOTAI_H
