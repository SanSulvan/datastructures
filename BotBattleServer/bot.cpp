#include "bot.h"
#include "graphics.h"
#include "world.h"

#include <iostream>
#include <algorithm>

double subtractAngles(double a1, double a2);
double negateAngle(double angle);

const double shotEffectTime = 1.0;

BotEvent::BotEvent(BotEventType eventType, double eventTime, double collisionAngle, double moveDistance, const CameraView& cv)
{
    this->eventType = eventType;
    this->eventTime = eventTime;
    this->collisionAngle = collisionAngle;
    this->travelDistance = moveDistance;
    view = cv;
}

void write(CameraView& view, std::ostream& strm)
{
    strm << " " << (int)view.color.size() << " ";
    for (size_t i=0;i<view.color.size();i++)
    {
        if (view.color[i] == 0)
        {
            strm << "_";
        }
        else
        {
            strm << view.color[i];
        }
    }
}

void BotEvent::write(std::ostream& strm)
{
    switch (eventType)
    {
    case BotEventType::TurnComplete:
        strm << "Turned ";
        strm << eventTime;
        break;
    case BotEventType::MoveComplete:
        strm << "Moved ";
        strm << eventTime << " " << travelDistance;
        break;
    case BotEventType::ShotFired:
        strm << "Fired ";
        strm << eventTime;
        break;
    case BotEventType::MoveBlocked:
        strm << "Blocked ";
        strm << eventTime << " " << travelDistance;
        break;
    case BotEventType::ScanComplete:
        strm << "Scanned ";
        strm << eventTime;
        ::write(view, strm);
        break;
    case BotEventType::BotCollision:
        strm << "Bonk ";
        strm << eventTime << " " << collisionAngle << " " << travelDistance;
        break;
    case BotEventType::WallCollision:
        strm << "Wall ";
        strm << eventTime << " " << collisionAngle << " " << travelDistance;
        break;
    case BotEventType::BulletCollision:
        strm << "Shot ";
        strm << eventTime << " " << collisionAngle << " " << travelDistance;
        break;
    }

    strm << "\r\n";
}

/*
BotEvent BotEvent::read(std::istream& strm)
{
    BotEventType eventType;

    std::string et;

    strm >> et;

    if (et == "Turned")       { eventType = BotEventType::TurnComplete; }
    else if (et == "Moved")   { eventType = BotEventType::MoveComplete; }
    else if (et == "Fired")   { eventType = BotEventType::ShotFired; }
    else if (et == "Blocked") { eventType = BotEventType::MoveBlocked; }
    else if (et == "Scanned") { eventType = BotEventType::ScanComplete; }
    else if (et == "Bonk")    { eventType = BotEventType::BotCollision; }
    else if (et == "Wall")    { eventType = BotEventType::WallCollision; }
    else if (et == "Shot")    { eventType = BotEventType::BulletCollision; }
    else
    {
        // error
        return BotEvent(BotEventType::MoveBlocked, 0);
    }

    double eventTime;
    double collisionAngle = 0;
    CameraView view;

    strm >> eventTime;

    switch (eventType)
    {
    case BotEventType::TurnComplete:
    case BotEventType::MoveComplete:
    case BotEventType::ShotFired:
    case BotEventType::MoveBlocked:
        // nothing else to read
        break;
    case BotEventType::ScanComplete:
        ::read(view, strm);
        break;
    case BotEventType::BotCollision:
    case BotEventType::WallCollision:
    case BotEventType::BulletCollision:
        strm >> collisionAngle;
        break;
    }

    return BotEvent(eventType, eventTime, collisionAngle, view);
}
*/

/*

Events:

Standard events:

Move ended
Move blocked

Turn ended
Scan complete
Fire complete

Unexpected events:

Collided (direction?)
Shot


*/

template<class T>
T constrain(T value, T minValue, T maxValue)
{
    if (minValue > maxValue)
    {
        std::swap(minValue, maxValue);
    }

    if (value < minValue)
    {
        value = minValue;
    }
    if (value > maxValue)
    {
        value = maxValue;
    }
    return value;
}

int colorD(double r, double g, double b)
{
    int rcomp = constrain((int)(0xFF * constrain(r, 0.0, 1.0)), 0, 255);
    int gcomp = constrain((int)(0xFF * constrain(g, 0.0, 1.0)), 0, 255);
    int bcomp = constrain((int)(0xFF * constrain(b, 0.0, 1.0)), 0, 255);

    return rcomp << 16 | gcomp << 8 | bcomp;
}

// return 0-1 depending on where current is in range beginValue to endValue
double progress(double beginValue, double endValue, double current)
{
    current = constrain(current, beginValue, endValue);
    return (current-beginValue)/(endValue - beginValue);
}

double interpolate(double beginValue, double endValue, double t)
{
    return beginValue + (endValue - beginValue) * t;
}

BotCmd::BotCmd()
{
    lastApplyTime    = 0.0;
    totalElapsedTime = 0.0;
}

void BotCmd::apply(Bot * /*bot*/, double currentTime, double elapsedTime)
{
    lastApplyTime = currentTime;
    totalElapsedTime += elapsedTime;
}


class BotCmdNull : public BotCmd
{
public:
    BotCmdNull();
    void apply(Bot* bot, double currentTime, double elapsedTime);
    bool isComplete();
    bool isInterruptable();
    void getCmdColor(Bot* bot, int& circleColor, int& bodyColor);
    std::string name() { return "Null"; }
    bool nextStateIsNull() { return true; }
};

BotCmdNull::BotCmdNull()
{

}

void BotCmdNull::getCmdColor(Bot* /* bot */, int& circleColor, int& bodyColor)
{
    circleColor = 0xFFFFFF;
    bodyColor   = colorD(1, 1, 1);
}


void BotCmdNull::apply(Bot* bot, double currentTime, double elapsedTime)
{
    BotCmd::apply(bot, currentTime, elapsedTime);
}

bool BotCmdNull::isComplete()
{
    return true;
}

bool BotCmdNull::isInterruptable()
{
    return false;
}

class BotCmdIdle : public BotCmd
{
public:
    BotCmdIdle();
    void apply(Bot* bot, double currentTime, double elapsedTime);
    bool isComplete();
    bool isInterruptable();
    void getCmdColor(Bot* bot, int& circleColor, int& bodyColor);
    bool nextStateIsNull() { return true; }
    std::string name() { return "Idle"; }
};

BotCmdIdle::BotCmdIdle()
{

}

void BotCmdIdle::getCmdColor(Bot* /* bot */, int& circleColor, int& bodyColor)
{
    circleColor = 0xFFFFFF;
    bodyColor   = colorD(1, 1, 1);
}


void BotCmdIdle::apply(Bot* bot, double currentTime, double elapsedTime)
{
    BotCmd::apply(bot, currentTime, elapsedTime);
}

bool BotCmdIdle::isComplete()
{
    return true;
}

bool BotCmdIdle::isInterruptable()
{
    return false;
}

class BotCmdMove : public BotCmd
{
    double moveSpeed;
    double moveTime;
public:
    BotCmdMove(double speed, double time);
    void apply(Bot* bot, double currentTime, double elapsedTime);
    bool isComplete();
    bool isInterruptable();
    void getCmdColor(Bot* bot, int& circleColor, int& bodyColor);
    std::string name() { return "Move"; }
};

BotCmdMove::BotCmdMove(double speed, double time)
{
    moveSpeed = speed;
    moveTime  = time;

}

void BotCmdMove::getCmdColor(Bot* /* bot */, int& circleColor, int& bodyColor)
{
    circleColor = 0x00FF00;
    bodyColor   = colorD(0, 1, 0);
}

void BotCmdMove::apply(Bot* bot, double currentTime, double elapsedTime)
{
    if (isComplete())
    {
        return;
    }


    BotCmd::apply(bot, currentTime, elapsedTime);

    double angle = bot->direction;
    double remainingTime = moveTime - totalElapsedTime;
    if (remainingTime <= 0)
    {
        remainingTime = 0;
    }
    double useTime = std::min(elapsedTime, remainingTime);
    double dist  = moveSpeed * useTime;

    bot->execMove(currentTime, angle, dist, isComplete());
}

bool BotCmdMove::isComplete()
{
    return totalElapsedTime >= moveTime;
}

bool BotCmdMove::isInterruptable()
{
    return true;
}

class BotCmdTurn : public BotCmd
{
    double origTurnAngle;
    double remainingTurnAngle;
public:
    BotCmdTurn(double angle);
    void apply(Bot* bot, double currentTime, double elapsedTime);
    bool isComplete();
    bool isInterruptable();
    void getCmdColor(Bot* bot, int& circleColor, int& bodyColor);
    std::string name() { return "Turn"; }
};

BotCmdTurn::BotCmdTurn(double angle)
{
    remainingTurnAngle = angle;
    origTurnAngle = angle;
}

void BotCmdTurn::getCmdColor(Bot* /* bot */, int& circleColor, int& bodyColor)
{
    double cmdProgress = progress(origTurnAngle, 0, remainingTurnAngle);
    circleColor = 0xFF00FF;
    bodyColor   = colorD(1, 1-cmdProgress, 1);
}

void BotCmdTurn::apply(Bot* bot, double currentTime, double elapsedTime)
{
    if (isComplete())
    {
        return;
    }
    
    BotCmd::apply(bot, currentTime, elapsedTime);

    if (remainingTurnAngle > 0)
    {
        double rate = 1.0;
        double amount = elapsedTime * rate;
        if (amount >= remainingTurnAngle)
        {
            amount = remainingTurnAngle;
            remainingTurnAngle = 0;
        }
        else
        {
            remainingTurnAngle -= amount;
        }
        bot->execTurn(currentTime, amount, isComplete());
    }
    else if (remainingTurnAngle < 0)
    {
        double rate = -1.0;
        double amount = elapsedTime * rate;
        if (amount <= remainingTurnAngle)
        {
            amount = remainingTurnAngle;
            remainingTurnAngle = 0;
        }
        else
        {
            remainingTurnAngle -= amount;
        }
        bot->execTurn(currentTime, amount, isComplete());
    }
}

bool BotCmdTurn::isComplete()
{
    return remainingTurnAngle == 0;
}

bool BotCmdTurn::isInterruptable()
{
    return false;
}

class BotCmdFire : public BotCmd
{
    double timeToFire;
public:
    BotCmdFire();
    void apply(Bot* bot, double currentTime, double elapsedTime);
    bool isComplete();
    bool isInterruptable();
    void getCmdColor(Bot* bot, int& circleColor, int& bodyColor);
    std::string name() { return "Fire"; }
};

BotCmdFire::BotCmdFire()
{
    timeToFire = 1;
}

void BotCmdFire::getCmdColor(Bot* /* bot */, int& circleColor, int& bodyColor)
{
    double cmdProgress = progress(0, timeToFire, totalElapsedTime);
    circleColor = 0xFF0000;
    bodyColor   = colorD(1, 1-cmdProgress, 1-cmdProgress);
}

void BotCmdFire::apply(Bot* bot, double currentTime, double elapsedTime)
{
    if (isComplete())
    {
        return;
    }
    
    BotCmd::apply(bot, currentTime, elapsedTime);
    
    if (isComplete())
    {
        bot->execFire(currentTime);
    }
}

bool BotCmdFire::isComplete()
{
    return totalElapsedTime >= timeToFire;
}

bool BotCmdFire::isInterruptable()
{
    return false;
}

class BotCmdScan : public BotCmd
{
    double scanTime;
    double scanAngle;
public:
    BotCmdScan(double angle);
    void apply(Bot* bot, double currentTime, double elapsedTime);
    bool isComplete();
    bool isInterruptable();
    void getCmdColor(Bot* bot, int& circleColor, int& bodyColor);
    std::string name() { return "Scan"; }
};

BotCmdScan::BotCmdScan(double angle)
{
    scanAngle = angle;
    scanTime  = angle;
}

void BotCmdScan::getCmdColor(Bot* bot, int& circleColor, int& bodyColor)
{
    double cmdProgress = progress(0, scanTime, totalElapsedTime);
    circleColor = 0x0000FF;
    bodyColor   = colorD(1-cmdProgress, 1-cmdProgress, 1);

    std::vector<Vec2d> scanPoints;

    auto position = bot->getPos();
    auto direction = bot->direction;
    auto diameter = bot->diameter;

    scanPoints.push_back(position);

    double startAngle = direction-scanAngle/2;
    double endAngle   = direction+interpolate(-scanAngle/2, scanAngle/2, cmdProgress);
    double step = (endAngle - startAngle) / 20;

    if (step < MY_PI/180)
    {
        Vec2d scanEnd   { diameter * 5, 0 };
        scanEnd.rotate(startAngle);
        scanEnd.translate(position);

        drawLine(position, scanEnd, 0x0000FF);
    }
    else
    {
        for (double a = startAngle; a <= endAngle; a += step)
        {
            Vec2d scanEnd   { diameter * 5, 0 };
            scanEnd.rotate(a);
            scanEnd.translate(position);
            scanPoints.push_back(scanEnd);
        }

        drawPolygon(scanPoints, 0x0000FF);
    }
}

void BotCmdScan::apply(Bot* bot, double currentTime, double elapsedTime)
{
    if (isComplete())
    {
        return;
    }
    
    BotCmd::apply(bot, currentTime, elapsedTime);
    
    if (isComplete())
    {
        bot->execScan(currentTime, scanAngle);
    }
}

bool BotCmdScan::isComplete()
{
    return totalElapsedTime >= scanTime;
}

bool BotCmdScan::isInterruptable()
{
    return false;
}

Bullet::Bullet(Vec2d pos, double direction, Vec2d vel)
{
    position = pos;
    velocity = vel;
    collided = false;
    angle = direction;
}

void Bullet::draw()
{
    fillEllipse(position,5,5,0,0xFF8888);
}

bool Bullet::collidesWith(Bot& bot)
{
    return (position - bot.position).magnitude() < bot.radius();
}

void Bullet::update(double elapsedTime)
{
    position = position + velocity * elapsedTime;
}


Bot::Bot(World *world, BotAI *brain)
{
    this->botId = world->availableBotId();

    this->world = world;
    this->brain = brain;

    color = 0x000000;
    diameter = 40;
    direction = 0;

    command = new BotCmdIdle();

    collided = false;
    collideEffect = 0.0;
    shotEffect    = 0.0;

    health = 3;

    disconnected = false;
}

Bot::~Bot()
{
    delete brain;
}

void Bot::setPos(Vec2d pos)
{
    position = pos;
    moveStartPos = pos;
    moveEndPos = pos;
}

void Bot::execFire(double eventTime)
{
    moveStartPos = position;
    world->fire(position, direction, radius()*1.1);
    events.push_back({BotEventType::ShotFired, eventTime, direction, 0});
}

void Bot::execMove(double eventTime, double angle, double distance, bool isComplete)
{
    auto delta = Vec2d(distance, 0).rotated(angle);

    position = position + delta;

    if (isComplete)
    {
        events.push_back({BotEventType::MoveComplete, eventTime, 0, (position - moveStartPos).magnitude()});
    }
}

void Bot::execTurn(double eventTime, double angle, bool isComplete)
{
    moveStartPos = position;

    direction += angle;
    
    while (direction < 0)
    {
        direction += 2*MY_PI;
    }
   
    while (direction > 2*MY_PI)
    {
        direction -= 2*MY_PI;
    }

    if (isComplete)
    {
        events.push_back({BotEventType::TurnComplete, eventTime, 0, 0});
    }
}

void Bot::execScan(double eventTime, double fov)
{
    moveStartPos = position;
    auto view = world->look(fov, position, direction);

    events.push_back({BotEventType::ScanComplete, eventTime, direction, 0, view});
}

void Bot::gotShot(double eventTime, double bulletAngle)
{
    if (health == 0)
    {
        return;
    }

    health -= 1;

    shotEffect = (health == 0) ? shotEffectTime*2 : shotEffectTime;

    double collisionAngle = negateAngle(subtractAngles(bulletAngle, direction));

    collideAngle = collisionAngle;

    events.push_back({BotEventType::BulletCollision, eventTime, collisionAngle, (position - moveStartPos).magnitude()});
}

void Bot::hitWall(double eventTime, double wallNormalAngle)
{
    collided = true;

    double collisionAngle = subtractAngles(negateAngle(wallNormalAngle), direction);

    collideAngle = collisionAngle;

    events.push_back({BotEventType::WallCollision, eventTime, collisionAngle, (position - moveStartPos).magnitude()});
}

void Bot::hitBot(double eventTime, double otherBotDirection)
{
    collided = true;

    double collisionAngle = subtractAngles(otherBotDirection, direction);

    collideAngle = collisionAngle;

    events.push_back({BotEventType::BotCollision, eventTime, collisionAngle, (position - moveStartPos).magnitude()});
}

bool Bot::collidesWith(Bot& otherBot, double margin)
{
    return (position - otherBot.position).magnitude() < (diameter/2 + otherBot.diameter/2 + margin);
}

void Bot::gotStopped(double eventTime)
{
    position = lastPos;
    collideEffect = 0.5;
    auto numEvents = events.size();
    events.erase(std::remove_if(events.begin(), events.end(),
       [](BotEvent& e) { return e.eventType == BotEventType::MoveComplete; }), events.end());
    if (numEvents != events.size())
    {
        std::cout << "This happened\n";
    }

    events.push_back({BotEventType::MoveBlocked, eventTime, 0, (position - moveStartPos).magnitude()});
    giveCommand(BotCmdType::Idle, 0, 0);
}

void Bot::useBrainCommand()
{
    BotCmdType cmd;
    double arg1;
    double arg2;
    brain->getCmd(cmd, arg1, arg2);
    giveCommand(cmd, arg1, arg2);
}

void Bot::giveCommand(BotCmdType cmd, double arg1, double arg2)
{
    bool debug = false;

    switch (cmd)
    {
    case BotCmdType::NoCommand:
        break;
    case BotCmdType::Disconnected:
        if (debug) { std::cout << "Bot: " << botId << " gave command: Disconnected\n"; }
        break;
    case BotCmdType::Fire:
        if (debug) { std::cout << "Bot: " << botId << " gave command: Fire\n"; }
        delete command;
        command = new BotCmdFire();
        break;
    case BotCmdType::Idle:
        if (debug) { std::cout << "Bot: " << botId << " gave command: Idle\n"; }
        delete command;
        command = new BotCmdIdle();
        break;
    case BotCmdType::Look:
        if (debug) { std::cout << "Bot: " << botId << " gave command: Look " << arg1 << "\n"; }
        delete command;
        command = new BotCmdScan(arg1);
        break;
    case BotCmdType::Move:
        if (debug) { std::cout << "Bot: " << botId << " gave command: Move " << arg1 << " " << arg2 << "\n"; }
        delete command;
        moveStartPos = position;
        command = new BotCmdMove(arg1, arg2);
        break;
    case BotCmdType::Turn:
        if (debug) { std::cout << "Bot: " << botId << " gave command: Turn " << arg1 << "\n"; }
        delete command;
        command = new BotCmdTurn(arg1);
        break;
    }
}

void Bot::update(World& /* world */, double currentTime, double elapsedTime) // return true if last command complete
{
    if (brain->isDisconnected())
    {
        std::cout << "Brain disconnected\n";
        disconnected = true;
        return;
    }

    lastPos = position;

    command->apply(this, currentTime, elapsedTime);

    if (command->isComplete())
    {
        if (command->nextStateIsNull())
        {
            delete command;
            command = new BotCmdNull();
        }
        else
        {
            //std::cout << botId << " Command: " << command->name() << " Complete\n";
            delete command;
            command = new BotCmdIdle();
        }
    }
}

void Bot::processEvents(double currentTime)
{
    bool debug = false;

    if (events.size() > 0)
    {
        if (debug)
        {
            for (BotEvent& event : events)
            {
                std::cout << "Bot " << botId << " handling event: ";
                event.write(std::cout);
            }
        }


        brain->handleEvents(events, currentTime);

        events.clear();
    }

    brain->asyncWait();
    useBrainCommand();
    brain->ResetCmd();
}


void Bot::draw(double elapsedTime)
{
    Vec2d front{ diameter/2, 0 };
    Vec2d back1{ diameter/2, 0 };
    Vec2d back2{ diameter/2, 0 };

    front.rotate(direction);
    back1.rotate(direction+MY_PI-MY_PI/6);
    back2.rotate(direction+MY_PI+MY_PI/6);

    int bodyColor   = 0xFFFFFF;
    int circleColor = 0xFFFFFF;

    command->getCmdColor(this, circleColor, bodyColor);

    fillPolygon({front+position, back1+position, back2+position}, bodyColor);

    drawEllipse(position, diameter, diameter, 0, circleColor);

    bool showCollideDir = false;

    if (collideEffect > 0)
    {
        showCollideDir = true;

        drawEllipse(position, diameter*1.1, diameter*1.1, 0, 0xFFFFFF);
        collideEffect -= elapsedTime;
        if (collideEffect < 0)
        {
            collideEffect = 0;
        }
    }

    if (shotEffect > 0)
    {
        showCollideDir = true;

        double explodeDiameter = interpolate(diameter, diameter*2, progress(shotEffectTime, 0, shotEffect));

        for (double d = 1.0; d <= explodeDiameter; d += 4.0)
        {
            drawEllipse(position, d, d, 0, 0xFF0000);
        }

        shotEffect -= elapsedTime;

        if (shotEffect < 0)
        {
            shotEffect = 0;
        }
    }

    if (showCollideDir)
    {
        // draw last collision angle
        Vec2d collideVector{ diameter, 0 };
        collideVector.rotate(direction + collideAngle);
        drawLine(position, collideVector + position, 0xFF8888);
    }

    if (!isDead())
    {
        fillEllipse(position, diameter, diameter, 0, color);
    }

    drawString(position + Vec2d { 15, 15}, brain->botName, 0.1, 0xFFFFFF);
}

