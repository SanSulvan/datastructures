#ifndef PRIORITYQUEUE_H
#define PRIORITYQUEUE_H

#include <iostream>
#include <vector>

class QueueItem
{
public:
    double      priority;
    std::string value;
    bool isHigherPriority(QueueItem item);
};

class PriorityQueue
{
private:
    std::vector<QueueItem> queue;

public:
    PriorityQueue();
   ~PriorityQueue();
    void insert(QueueItem item);
    QueueItem remove();
    int size();

    // Helper funcitons
    QueueItem& getItem(unsigned int pos);
    int findParent(int position);
    int getLeftChild(unsigned int position);
    int getRightChild(unsigned int position);
    bool isHigherPriorityThan(int pos1, int pos2);
    void swap(int& pos1, int& pos2);
};

#endif // PRIORITYQUEUE_H
