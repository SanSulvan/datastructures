#ifndef SpencerAI_H
#define SpencerAI_H

#include "botai.h"

class SpencerAI : public BotAI
{
public:
    SpencerAI();

    void whenShotFired(double elapsedTime);
    void whenScanCompleted(double elapsedTime, std::vector<int> view);
    void whenTurnCompleted(double elapsedTime);
    void whenMoveBlocked(double elapsedTime);
    void whileIdle(double elapsedTime);
    void whileMoving(double elapsedTime);
};

#endif // SpencerAI_H
