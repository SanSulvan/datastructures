#include "vec2d.h"
#include "ET_AI.h"
double specialAngle = 0.0183273;

ET_AI::ET_AI()
{

}

/*

Shoot();
Scan(fieldOfView); // maximum field of view is given by maxFieldOfView function
Move(speed);       // maximum speed is given by maxSpeed function
Turn(radians);

*/

void ET_AI::whenShotFired(double /* elapsedTime */)
{
    Scan(specialAngle);
}

void ET_AI::whenScanCompleted(double /* elapsedTime */, std::vector<int> view)
{
    if (view[view.size()/2.0] != 0x000000)
    {
        Fire();
    }
    else
    {
        int sawSomething1 = -1;
        int sawSomething2 = -1;

        for (size_t i = 0; i < view.size(); ++i)
        {
            if (view[i] != 0x000000)
            {
                if (sawSomething1 == -1)
                {
                    sawSomething1 = i;
                }
                else
                {
                    sawSomething2 = i;
                }
            }
        }

        int avg = (sawSomething1 + sawSomething2)/2;

        if (avg < (int)view.size()/2)
        {
            Turn(2*specialAngle);
        }
        else
        {
            Turn(-2*specialAngle);
        }
    }
}

void ET_AI::whenTurnCompleted(double /* elapsedTime */)
{
    Scan(specialAngle);
}

void ET_AI::whenMoveBlocked(double /* elapsedTime */)
{
}

void ET_AI::whileIdle(double /* elapsedTime */)
{
    Scan(specialAngle);
}

void ET_AI::whileMoving(double /* elapsedTime */)
{

}
