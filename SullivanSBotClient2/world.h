#ifndef WORLD_H
#define WORLD_H

#include <vector>
#include "vec2d.h"

#include <QMutex>
#include <QMutexLocker>
#include <QTcpSocket>
#include "botai.h"

class World : public QObject
{
    Q_OBJECT

    std::vector<int>    lastView;

    std::vector<BotEvent> events;

    BotAI myAi;

    double fieldTop;
    double fieldBottom;
    double fieldLeft;
    double fieldRight;

    QMutex mutex;

    QTcpSocket *socket;
    std::string receivedData;
    std::vector<std::string> commandQueue;
    bool gotDisconnected;


public:
    World(double fieldTop, double fieldBottom, double fieldLeft, double fieldRight, QObject* parent);
   ~World();
    void draw(double elapsedTime);

    void update();

    bool isDisconnected() { return gotDisconnected; }

private:
    void sendData(std::string data);
    void handleResponse(std::string cmd);

public slots:

    void readyRead();
    void disconnected();
    void socketStateChanged(QAbstractSocket::SocketState);
    void socketError(QAbstractSocket::SocketError);

};

#endif // WORLD_H
