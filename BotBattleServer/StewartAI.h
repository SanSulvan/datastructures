#ifndef STEWARTAI_H
#define STEWARTAI_H

#include "botai.h"

class StewartAI : public BotAI
{
public:
    StewartAI();

    void whenShotFired(double elapsedTime);
    void whenScanCompleted(double elapsedTime, std::vector<int> view);
    void whenTurnCompleted(double elapsedTime);
    void whenMoveBlocked(double elapsedTime);
    void whileIdle(double elapsedTime);
    void whileMoving(double elapsedTime);
};

#endif // STEWARTAI_H
