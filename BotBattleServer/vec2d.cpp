#include "vec2d.h"
#include <cmath>

Vec2d::Vec2d()
{
    x = 0;
    y = 0;
}

Vec2d::Vec2d(double xvalue, double yvalue)
{
    x = xvalue;
    y = yvalue;
}

double Vec2d::magnitude()
{
    return sqrt(x*x + y*y);
}

void Vec2d::scale(double s)
{
    x *= s;
    y *= s;
}

void Vec2d::rotate(double radians)
{
    *this = { x * cos(radians) - y * sin(radians), x * sin(radians) + y * cos(radians) };
}

void Vec2d::translate(Vec2d offset)
{
    x += offset.x;
    y += offset.y;
}

bool Vec2d::equals(Vec2d other, double threshold)
{
    return fabs(x - other.x) < threshold && fabs(y - other.y) < threshold;
}

Vec2d operator-(Vec2d p1, Vec2d p2)
{
    return Vec2d {p1.x - p2.x, p1.y - p2.y };
}

Vec2d operator+(Vec2d p1, Vec2d p2)
{
    return Vec2d {p1.x + p2.x, p1.y + p2.y };
}

Vec2d operator*(Vec2d v, double s)
{
    return Vec2d { v.x * s, v.y * s };
}

Vec2d operator*(double s, Vec2d v)
{
    return Vec2d { v.x * s, v.y * s };
}


