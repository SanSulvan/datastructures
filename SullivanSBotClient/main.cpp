#include "vec2d.h"
#include "graphics.h"
#include <cmath>
#include <vector>
#include <iostream>
#include <sstream>
#include "ImageDraw.h"
#include <cstdlib>
#include <ctime>
#include <limits>
#include "world.h"
#include "mymath.h"

#include "internal.h"

using namespace std;

Vec2d mousePos;  // set automatically to the current mouse location

int windowWidth  = 1000;
int windowHeight = 500;

double fieldTop    = windowHeight/2-10;
double fieldBottom = -windowHeight/2+10;
double fieldLeft   = -windowWidth/2+10;
double fieldRight  = windowWidth/2-10;

void initializeRandomNumberGenerator() // put this in your main
{
    srand(time(0));
}

int generateRandom(int maxValue)  // generate a number between 0 and maxValue
{
    return rand()%(maxValue+1);
}



double cameraAngle = 3.0/4;



World* world = nullptr;


void updateDisplay(QObject* parent, int elapsedMs)
{
    if (!world)
    {
        world = new World(fieldTop, fieldBottom, fieldLeft, fieldRight, parent);
    }

    double elapsedTime = elapsedMs / 1000.0;
    world->update();
    world->draw(elapsedTime);

}

void keyboardFunc(unsigned char key, int /*x*/, int /*y*/)
{
    switch (key)
    {
    case 'w':
        cameraAngle += 0.1;
        if (cameraAngle >= 2*MY_PI)
        {
            cameraAngle -= 2*MY_PI;
        }
        break;
    case 'a':
        cameraAngle -= 0.1;
        if (cameraAngle < 0)
        {
            cameraAngle += 2*MY_PI;
        }
        break;
    case 'd':
        break;
    case 's':
        break;
    }
}

void keyboardUpFunc(unsigned char /* key */, int /*x*/, int /*y*/)
{
}

void mouseFunc(int /*button*/, int /*state*/, int /*x*/, int /*y*/)
{
}

void motionFunc(int /*x*/, int /*y*/)
{
}

void passiveMotionFunc(int /*x*/, int /*y*/)
{
}



void beginGraphicsLoop(const std::string &name, int windowWidth, int windowHeight, int frameRate)
{
    int argc = 0;
    char **argv = 0;

    QApplication app(argc, argv);

    Window w(name, windowWidth+2, windowHeight+2, frameRate);

    w.resize(QSize(windowWidth+2, windowHeight+2));
    w.show();

    app.exec();
}

int main()
{
    initializeRandomNumberGenerator();

    beginGraphicsLoop("Bot Battle Client", windowWidth, windowHeight, 24);

    return 0;
}
