#pragma once

#include <string>
#include "vec2d.h"

struct RGBPixel
{
	unsigned char r;
	unsigned char g;
	unsigned char b;
    unsigned char a;
};

class QImage;

class Image
{
private:
    QImage      *_img;
    unsigned int _texture_id;
public:
    Image(int width, int height);
    Image(const std::string &filename);
   ~Image();
    RGBPixel &pixel(int x, int y);
    void load(const std::string &filename);
    void save(const std::string &filename);
    int width();
    int height();
    void draw(Vec2d position, double z, double w, double h);
};

