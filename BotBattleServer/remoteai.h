#ifndef REMOTEAI_H
#define REMOTEAI_H

#define _USE_MATH_DEFINES

#include <QThread>
#include <QTcpSocket>
#include <sstream>

#include "botai.h"


class RemoteAI: public BotAI
{
    Q_OBJECT

    QTcpSocket *socket;
    qintptr     socketDescriptor;
    QByteArray  data;

public:

    RemoteAI(qintptr socketId);
   ~RemoteAI();

    void handleEvents(std::vector<BotEvent> events, double currentTime);

    void asyncWait();

private:
    void sendData(QByteArray data);
    bool handleCommand();
    void handleCommand(std::string cmd);

public slots:

    void readyRead();
    void disconnected();
    void socketStateChanged(QAbstractSocket::SocketState);
    void socketError(QAbstractSocket::SocketError);
};

#endif // REMOTEAI_H
