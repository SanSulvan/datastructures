#include <iostream>
#include "priorityqueue.h"

using namespace std;

int main()
{
    PriorityQueue pq;

    pq.insert(QueueItem { 1, "One"});
    pq.insert(QueueItem { 3, "Three"});
    pq.insert(QueueItem { 2, "Two"});
    pq.insert(QueueItem { 7, "Seven"});

    cout << "Size = " << pq.size() << "  should be 4\n";

    cout << "First value is " << pq.getItem(0).value << "\n";
    cout << "Second value is " << pq.getItem(1).value << "\n";
    cout << "Third value is " << pq.getItem(2).value << "\n";
    cout << "Fourth value is " << pq.getItem(3).value << "\n";

    // ^^^ This all works!

    QueueItem i1 = pq.remove();
    cout << "Value is: " << i1.value << endl;
    cout << "Size = " << pq.size() << "  should be 3\n";

    QueueItem i2 = pq.remove();
    cout << "Value is: " << i2.value << endl;
    cout << "Size = " << pq.size() << "  should be 2\n";

    QueueItem i3 = pq.remove();
    cout << "Value is: " << i3.value << endl;
    cout << "Size = " << pq.size() << "  should be 1\n";

    QueueItem i4 = pq.remove();
    cout << "Value is: " << i4.value << endl;
    cout << "Size = " << pq.size() << "  should be 0\n";

    system("pause");

    return 0;
}
