#include "vec2d.h"
#include "graphics.h"
#include <cmath>
#include <vector>
#include <iostream>
#include <sstream>
#include "ImageDraw.h"
#include <cstdlib>
#include <ctime>
#include <algorithm>

using namespace std;

void initializeRandomNumberGenerator() // put this in your main
{
    srand(time(0));
}

int generateRandom(int maxValue)  // generate a number between 0 and maxValue
{
    return rand()%(maxValue+1);
}

using namespace std;

Vec2d mousePos;  // set automatically to the current mouse location

class CPoint
{
public:
    Vec2d location;
    int   color;
};

std::vector<CPoint> points;
std::vector<CPoint> convexHull;
CPoint mousePosition;

bool compareX(const CPoint& p1, const CPoint& p2)
{
    return p1.location.x < p2.location.x;
}

void sortByX(std::vector<CPoint>& points)
{
    std::sort(points.begin(), points.end(), compareX);
}

bool arePointsEqual(CPoint point1, CPoint point2)
{
    if (point1.location.x == point2.location.x && point2.location.y == point1.location.y)
    {
        return true;
    }
    return false;
}

double findAngleTwoVectors(Vec2d vec1, Vec2d vec2)
{
    double dotProduct = vec1.x*vec2.x+vec1.y*vec2.y;
    double angle = acos(dotProduct/(vec1.magnitude()*vec2.magnitude()));
    return angle;
}

bool isLeftSide(Vec2d vec1, Vec2d vec2)
{
    if (vec1.x*vec2.y-vec1.y*vec2.x > 0)
    {
        return true;
    }
    return false;
}

void findConvexHull(std::vector<CPoint> points)
{
    if (arePointsEqual(points[points.size()-2], points[points.size()-1]))
    {
        points.erase(points.begin()+points.size()-1);
    }
    CPoint currentPoint = points[points.size()-1];
    CPoint savePoint = currentPoint;
    Vec2d currentDirection;
    currentDirection.x = 0;
    currentDirection.y = 1;
    convexHull.clear();
    double iteratorLocation = points.size()-1;
    while (true)
    {
        convexHull.push_back(currentPoint);
        points.erase(points.begin()+iteratorLocation);
        vector<Vec2d> vectorValues;
        for (unsigned int i = 0; i < points.size(); i++)
        {
            vectorValues.push_back(points[i].location - currentPoint.location);
        }
        double currentAngle = 10;
        for (unsigned int i = 0; i < vectorValues.size(); i++)
        {
            double placeHolder = findAngleTwoVectors(vectorValues[i], currentDirection);
            if (placeHolder < currentAngle)
            {
                currentAngle = placeHolder;
                iteratorLocation = i;
            }
        }
        if (findAngleTwoVectors(currentDirection, savePoint.location - currentPoint.location) < currentAngle && findAngleTwoVectors(currentDirection, savePoint.location - currentPoint.location)!= 0 && isLeftSide(currentDirection, savePoint.location - currentPoint.location))
        {
            break;
        }
        currentDirection = vectorValues[iteratorLocation];
        currentPoint = points[iteratorLocation];
    }
    convexHull.push_back(savePoint);
}

void updateDisplay(int /* elapsedMs */)
{
    for (auto& p : points)
    {
        drawPoint(p.location, p.color);
    }

    drawPoint(mousePos, 0xFFFFFF);

    if (points.size() > 0)
    {
        mousePosition.location = mousePos;
        mousePosition.color = 0xFFFFFF;
        std::vector<CPoint> finalPoints = points;
        finalPoints.push_back(mousePosition);
        sortByX(finalPoints);
        findConvexHull(finalPoints);
        for (unsigned int i = 0; i < convexHull.size()-1; i++)
        {
            drawLine(convexHull[i].location, convexHull[i+1].location, 0xFF0000);
        }

        drawLine(convexHull[convexHull.size()-1].location, convexHull[0].location, 0xFF0000);
    }
}

void generatePoints(int count)
{
    points.clear();

    for (int i =0; i< count; i++)
    {
        CPoint p;
        p.color    = 0xFFFFFF; // generateRandom(0xFFFFFF);
        p.location.x = generateRandom(1000) - 500;
        p.location.y = generateRandom(500) - 250;

        points.push_back(p);
    }
}

void keyboardFunc(unsigned char key, int /*x*/, int /*y*/)
{
    switch (key)
    {
    case 'g':
        generatePoints(50);
        break;
    case 'a':
        break;
    case 'd':
        break;
    case 's':
        break;
    }
}

void keyboardUpFunc(unsigned char /* key */, int /*x*/, int /*y*/)
{
}

void mouseFunc(int /*button*/, int /*state*/, int /*x*/, int /*y*/)
{
}

void motionFunc(int /*x*/, int /*y*/)
{
}

void passiveMotionFunc(int /*x*/, int /*y*/)
{
}

int main()
{
    initializeRandomNumberGenerator();


    beginGraphicsLoop("Hello World", 1000, 500, 24);
    return 0;
}
