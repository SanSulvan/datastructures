TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    tests.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    catch.hpp

